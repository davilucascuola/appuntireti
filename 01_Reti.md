---
author: Giovanni Foletto
title: Appunti di Reti
subtitle: Sept. 2021 - Nov. 2021
rights: see LICENSE
ibooks:
  version: 1.0
---



# Reti 

## Abstract

>  Started: Martedì 14/09/2021

> Fabrizio Granelli - II anno ICE

Corso di base riguardante la rete moderna. Questo è un corso rimodulato e serve a conoscere *solo* le basi. 

Libro di riferimento: `Computer Networking: A Top-Down Approach, 7th Edition`.



## Indice

[TOC]

------



# Introduzione

<img src="image/image-20210929102946179.png" alt="image-20210929102946179" style="zoom:33%;" />

Internet è nato negli anni ‘80 con i primi protocolli e la prima forma di struttura. Ora internet fornisce un accesso a circa 4 miliardi di persone e che non prevedeva la compravendita online o qualsiasi cosa si può fare ora sulla rete. Vedremo quindi anche come si sono innovate le tecnologie per consentire a questa rete di lavorare con tecnologie diverse da quella per cui è stato creato.

Da molti anni la persona non è più il principale utente di internet, ma ci sono sempre più oggetti che popolano la rete. Ad esempio un tostapane intelligente, oppure una cornice web over-the-ip. Si possono avere dei web-server miniaturizzati che lavorano per la rete nella rete.

Internet si sta comportando come la rete elettrica: tutti la utilizzano mentre solo pochissimi sanno come effettivamente funziona. Di fatto, tutti accedono alla rete come se fosse una *black box*, ovvero che non si sa come lavori all’interno. Inoltre è uno strumento che cambia la vita.

Con Internet si apre anche la discussione sul *digital divide* ovvero la differenza tra le popolazioni che possono accedere a internet e quelle invece che non lo possono fare. Google aveva provato a proporre una soluzione a questo problema, attraverso il *Google Project Loon*, che ha fallito come progetto iniziale di portare internet in zone distanti dalla connettività a costi favorevoli, ma è servito ad esempio per fornire connessione durante i soccorsi al terremoto in Perù.

In Italia non c’è una struttura molto elaborata di internet, ma nonostante questo c’è un gruppo abbastanza fornito di esperti che si occupa di innovazione in questo campo, rendendo il nostro stato meta abbastanza favorevole per questo tipo di studi.

Le applicazioni di internet sono imprevedibili: era nata con scopo militare, poi passata alla ricerca e infine oggi viene utilizzata da tutti. Quindi non si sa mai come una struttura fallimentare in un certo campo, possa essere riscoperta perfettamente funzionante per altri scopi.



## Obiettivi

* introdurre la terminologia e i concetti di base sulle reti di telecomunicazioni. Questa parte può sembrare ripetitiva, ma vanno dichiarate per avere un campo comune. Si definirà la pila protocollare, ovvero il modo di organizzare la rete internet.
* fornire conoscenza su struttura e protocolli di internet
* approccio top-down, con lezioni e esercitazioni. Si parte da livello applicazione (l’utente finale) e si studieranno i protocolli di comunicazione che si rifanno a dei servizi standard. Questa sezione tratterà anche funzionamento di applicazioni (che siano peer-to-peer, server-client, o il cloud). C’è la necessità di conoscere questi protocolli, per consentire il corretto funzionamento con il massimo della resa, fare la scelta per il miglior protocollo per lo scopo cercato.



## Argomenti

* che cos’è Internet? 

  il suo funzionamento, cosa comporta e tutto il collegato.

* Livello di applicazione: studio dei servizi che si appoggiano alla rete

* Livello di trasporto: in particolare TCP e UDP

* Livello di Rete: come si trasmettono i dati nei canali

* Livello Fisico: (non trattato in questo corso) e serve a studiare la sequenza di bit che viene mandata attraverso la rete. Di solito questa parte è molto specializzata nel campo delle comunicazioni, richiedendo un corso unico e con tutta una teoria, sviluppata dagli anni ‘40 per trasformare un informazione in segnale capace di propagarsi. Di fatto questo layer di comunicazione trasforma le informazioni in onde elettrostatiche. 

  In questo argomento rientrano anche tutte le tecniche di comunicazione wireless, che sono molto più raffinate e complesse. 

Molti dei concetti che si trattano nella rete sono molto astratti, ma da alcuni anni si svolge un laboratorio per capire il funzionamento di base attraverso la virtualizzazione di alcuni apparati di rete (attraverso kathara.org). Questo software sviluppato da UniRoma3 attraverso Linux per simulare le reti. Soprattutto riesce a simulare il traffico tra router interno alla rete, ovvero non un router casalingo.

Questo software è un emulatore, quindi si può installare dei software come fossero reali e testare cosa entra e cosa esce.



(*Lezione 2*)

# Capitolo 1: Introduzione e nomenclatura

## 1.0 Panoramica e obiettivi

* introdurre la terminologia e i concetti di base
  * che cos’è Internet? Che cos’è un protocollo? 
  * ai confini della rete: host, reti di accesso e mezzi trasmissivi
  * nucledo della rete: commutazione di circuito e commutazione di pacchetto, struttura di Internet
  * prestazioni: ritardi, perdite e throughput
  * sicurezza
  * livelli di protocollo, modelli di servizio (sono necessari perché risolvendo un problema in piccoli problemi si riesce poi a risolvere problemi più grandi in modo più semplice)
  * un po’ di storia
* gli approfondimenti arriveranno nei capitoli successivi
* approccio: capire Internet attraverso degli esempi

Alcune scelte che sono presenti nella rete oggi dipendono dalla storia e dall’evoluzione, siccome non è mai stata spenta, e di conseguenza ha dovuto sempre rimanere funzionante. 

## 1.1 Termini Principali di internet

Per iniziare a spiegare internet, spieghiamo alcuni termini.

> **Host**
>
> Sistema terminale, il dispositivo che viene collegato alla rete. Ad esempio lo smartphone o il pc, ..
>
> La sua caratteristica principale è che gli host sono dei dispositivi reali e di uso comune (appunto, come un computer) e sono gli unici dispositivi sui quali girano le applicazioni (quindi può essere il mio computer o una macchina virtuale in un datacenter). 
>
> Di fatto sempre più spesso gli host ospitano le applicazioni, o la parte client o la parte server, e sono sempre meno sotto il controllo delle persone (ad esempio l’avvetto degli IOT devices).
>
> Gli host sono distaccati da come lavora l’applicazione, che può avere impostazioni di tipo client-server, oppure essere parte attiva del collegamento. Inoltre, di solito i server sono leggermente diversi dai normali computer perché hanno una capacità di calcolo superiore e una banda di accesso più elevata per garantire servizio a più persone.
>
> Questi oggetti si diversificano moltissimo per oggetti che si inseriscono in rete e di tecnologie di accesso alla rete.



> **Collegamenti**
>
> Sono le strutture, radio (onde elettromagnetiche o satellite) o fisiche (attraverso cavi di rame, fibra ottica o altro), che collegano più strutture fra loro. 
>
> I collegamenti sono importanti perché forniscono un salto (“*hop*”), ovvero un collegamento tra due segmenti di un percorso (Che può essere anche più lungo di un solo salto). Questi collegamenti non sono tutti uguali e dipendono completamente da chi costruisce questi collegamenti. Tutte le caratteristiche di questi collegamenti dipendono da come sono stati costruiti e da quali tecniche hanno deciso di utilizzare (ad esempio: la fibra di solito ha una certa banda di accesso a internet e un certo ping, ...).



> **Router**
>
> Sono i nodi che hanno come compito principale di inoltrare i dati da un punto a un altro. Sono gli effettivi smistatori del traffico di pacchetti creato nella rete. 
>
> La rete a questo punto è composta in maniera abbastanza uniforme. 



La rete di accesso è molto diversificabile sia per tecnologie utilizzate per accedere alla rete (rete mobile, rete aziendale, rete pubblica, rete telefonica, ...) sia per dispositivi che ci accedono.

<img src="image/image-20211001002809106.png" alt="image-20211001002809106" style="zoom:50%;" />

Le isole blu su questo schema di internet sono un concetto importante, infatti la rete nasce da una serie di gruppi di collegamento, che di solito sono uniti attraverso una serie di Internet Service Provider (**ISP**) via via più grosso (quindi internet è la fusione di diverse reti con un *sacco di proprietari*, e ogni isola è gestita in un modo con un certo grado di libertà, rispettando alcuni limiti che permettono alla propria “isola” di collegarsi alle altre). Si noti anche che il concetto di rete come collegamento di più host non viene meno, semplicemente che gli host rimangono collegati in piccola scala e collegati in gruppi sempre più grandi.

Ogni isola può essere gestita da chiunque la possieda con il solo vincolo di avere il router esterno (*gateway*, responsabile al collegamento degli host con la rete) compatibile con il resto della rete (per funzionare), ma lasciando il resto dell’organizzazione interna della rete completamente a discrezione del proprietario.

> **Internet**
>
> Internet è la rete delle reti, che attraverso una struttura ad albero si collega in tutta se stessa passando dal cuore (i rami principali e maggiori) fino all’utente finale.
>
> *internet* e *Intranet* sfruttano la stessa tecnologia, ma viene applicata a reti con scopi diversi (ad esempio: rete privata o rete aziendale o ancora una rete a accesso pubblico). Si differisce appunto il fatto che questa rete è “privata”/ha un proprietario dalla maiuscola. Non tutte queste reti sono collegate completamente con l’esterno, costruendola in modo che non tutto sia visibile dall’esterno normalmente.



> **Protocollo**
>
> Definizione del formato e dell’ordine dei messaggi scambiati fra due o più entità in comunicazione, così come le azioni intraprese in fase di trasmissione e/o ricezione di un messaggio o un altro evento (anche ad esempio nel caso di fallimento di comunicazione e le tecniche di riconnessione).
>
> Molto spesso la prima operazione è l’*hand-shake*, ovvero la stretta di mano tra i due componenti della comunicazione.
>
> [Come scrivere i messaggi, come devo ordinare e cosa vogliono dire e soprattutto se la serie di comunicazioni sono corrette]



Su Internet, **IEFT (Internet Engineering Task Force)** è un gruppo di controllo completamente democratico. Chiunque può lavorare in un loro working group per proporre un **RFC (Request for Comments)**, ovvero un documento aperto a tutti risultato di una serie di consultazioni che può portare alla creazione di un nuovo protocollo. 

Questi RFC sono in formato testo e sono pubblici, oltre che estremamente tecnici sui dati di riferimento forniti, siccome devono spiegare tutte le fasi di ogni protocollo. Questi protocolli sono aperti *ai commenti* perché si cerca sempre di migliorarsi. 

Alcuni protocolli hanno più di un RFC perché hanno avuto la necessità di più modifiche del protocollo.

Quando si sceglie di essere sulla rete, si possono ottenere principalmente due servizi:

* **affidabile/reliable** (nulla a che vedere con la sicurezza): il servizio fornisce la garanzia che la comunicazione non perda nessun bit e che si ottiene una copia bit-per-bit. 

  Questo servizio garantisce che tutto quello che viene ricevuto è la copia perfetta di quello che sto cercando.

  * Non viene indicato il tempo di questo trasferimento, infatti questo tipo di meccanismo perderà del tempo per garantire l’affidabilità

* **best-effort**: mando dei dati senza nessuna garanzia dell’effettiva ricezione dei dati. Questo servizio ha una serie di lati positivi rispetto all’altro servizio (anche se potrebbe non sembrare):
  
  * permette di bloccare tutti i servizi che garantiscono l’affidabilità, quindi di base ottenendo una perdita di tempo
  * per questo motivo si utilizza nella applicazioni che necessitano di un ritardo di risposta molto veloce (ad esempio i videogiochi online, oppure la voce). A questi sistemi non cambia molto la perdita del 5%-10% dei dati.
  * L’applicazione poi potrebbe compensare a questa perdita di dati/correzione di errori con delle tecniche di controllo implementate ad hoc via software.



## 1.2 Architettura di Internet

La rete è costituita da più tipi di elementi che si possono dividere come dispositivi di accesso, dispositivi fisici e dispositivi nel centro della rete.

### 1.2.1 Accesso alla rete

L’accesso alla rete viene definito come ultimo salto (o anche *last mile* o ultimo host) e si tratta dell’insieme di elementi che ci permette di essere presenti sulla rete.

La rete di solito rende visibili solo questi dispositivi: la rete effettiva creata da isole e da router rimane “oscurata”, mantenendo le tecnologie e il funzionamento di questi componenti nascosti e sempre funzionanti senza bisogno di un implementazione ad hoc.

Al bordo della rete sono presenti degli elementi che possono essere:

* sistemi terminali (**host**): responsabili dell’esecuzione dei programmi applicativi situati all’estremità di Internet.
* architettura **client/server**: l’host *client* richiede e riceve un servizio da un programma *server* in esecuzione su un altro terminale.

* servizio **peer-to-peer**: il servizio è direttamente collegato al computer di destinazione, che spesso è un host di tipo simile al nostro. Di fatto a questo tipo di tecnologia appartengono i torrent oppure (circa) anche il protocollo si Skype. Di solito questa modalità permette di avere un uso limitato o inesistente di server dedicati.

(Lezione 3)

L’applicazione di queste architetture di rete dipendono soltanto dall’applicazione utilizzata.

Spesso però ad oggi si parla di architettura client-server, oppure l’architettura **cloud** che è un server ancora più flessibile e grande. Infatti questi computer lavorano in modo da poter servire milioni di persone attivando più o meno computer in base alla richiesta e smistando il traffico attraverso tutti i server disponibili.



Quando si possiede un host, c’è la necessità di portare il collegamento al *primo nodo della rete*. Si parla in questo caso di **tecnologia di accesso**. Queste reti si differenziano spesso nella capacità di accesso (che in realtà è limitata alla capacità dell’ultimo miglio), e non dipende per nulla dalle altre performance della rete. 

A seconda della rete di accesso che si sceglie si può avere un collegamento **condiviso** o **dedicato**. Si parla di collegamento condiviso quando si utilizza il wi-fi come rete di accesso, oppure una rete aziendale. Si parla di collegamento dedicato quando ci si collega tramite ADSL (collegamento privato).



#### Reti di Accesso: accesso basato su cavo

Ci sono più modelli di accesso tramite cavo.

<img src="image/image-20211001084623911.png" alt="image-20211001084623911" style="zoom:50%;" />



Molto più comune in America, questo metodo permetteva a una singola centralina di fornire collegamento internet e televisione (la cosiddetta televisione via cavo) attraverso una sola connessione. Per trasmettere tutti questi dati assieme si utilizza il **FDM (Frequency Division Multiplexing)**, ovvero la tecnologia che permette di trasmettere in più frequenze (come la radio) e poi in base a questa frequenza si può ottenere il contenuto del canale. Per ottenere queste prestazioni si devono avere delle frequenze divise completamente, e devono essere anche segnali estraibili/divisibili rispetto agli altri. Al momento di ricezione si recupera questo segnale e si decodifica. Il funzionamento è uguale alla radio, con la differenza che la radio fornisce questo collegamento in modo analogico.



![image-20211001085226359](image/image-20211001085226359.png)

Lo stesso metodo di prima può essere migliorato utilizzando tecnologie come **HFC (Hybrid Fiber Coax)** che è un collegamento in fibra più performante, capace di trasmissioni fino a 40Mbps-1.2Gbps in downstream, 30-100Mbps in upstream.

Gli appartamenti condividono sempre la rete di accesso, siccome c’è un unico collegamento tra il CMTS e il servizio internet fornito dall’ISP.

 

![image-20211001085517918](image/image-20211001085517918.png)

Questo tipo di accesso, chiamato **DSL (Digital Subscriber Line)**. Ogni cavo che raggiunge la propria casa è del privato. Più connessioni private vengono poi aggregate nel *DSLAM (DSL Access Multiplexer)* che passa i dati alla rete dell’ISP. Questo metodo è utile perché i cavi utilizzati erano già in loco per la comunicazione telefonica, ma non può raggiungere alte prestazioni siccome il cavo non era originariamente pensato per svolgere questo compito. Si può comunque utilizzare per questo scopo, siccome il router di accesso casalingo sa adattare quello trasmissibile e capire il ricevuto.

Questo metodo però è molto limitato dal *digital divide*, infatti se il doppino è in brutte condizioni di manutenzione, o semplicemente si è troppo distanti dal DSLAM la linea DSL non funziona. Infatti questi collegamenti erano costruiti per far correre frequenze più basse, mentre trasmettendo frequenze così alte si ottiene uno smorzamento dell’onda più grande, riducendo la banda effettivamente distribuita.

I dati telefonici sono lo stesso presenti su questa linea, attraverso il multiplexing e al DSLAM vengono estratti e mandati sulla rete telefonica.

Il range della frequenza di trasmissione di questa connessione è tra i 24-52Mbps in downstream, 3.5-16Mbps in upstream.

Spesso questo servizio è offerto come **ADSL (Asymmetric DSL)**, chiamato in questo modo perché fornisce un banda in downstream molto maggiore di quella che fornisce in upstream.



<img src="image/image-20211001090653258.png" alt="image-20211001090653258" style="zoom:67%;" />

Anche le reti casalinghe sono ritenute reti di accesso, e sono molto comuni. In questo caso ho la necessità di tre apparecchi (di solito contenuti in un unico scatolotto) per avere connessione:

* modem via cavo o DSL
* componente router (di solito compreso di firewall e NAT)
* wifi access-point



Tipo di accesso **Wireless** fornisce una rete senza fili che può connettere più dispositivi. Tipicamente è un tipo di accesso preferibile all’interno degli edifici.

Si basa su uno standard chiamato *IEEE 802.11* con delle lettere a seguito che indicano la versione (siccome ancora attivamente sviluppato). L’ultima versione ad oggi è la *an* capace di circa 300/400Mbps. Le prestazioni migliorano ogni qualche anno perché le nuove tecnologie riescono a raggiungere frequenze più alte. Inoltre si riesce a tenere più bit/Hz, ovvero più informazioni condivise nella stessa frequenza. 

Un altra tecnologia di accesso wireless è il **Wide-area Cellular Access Networks**, ovvero la rete di accesso telefonica. Questa viene fornita dagli operatori telefonici con un raggio di anche qualche km (per questo si differisce dal wifi). Le reti cellulari fornivano una rete di accesso con una banda molto scadente, dopo il 4G-LTE è diventata un buon sostituto del wifi dato che la banda è compatibilmente accettabile (di solito 50-100Mbps).

 ![image-20211001092632330](image/image-20211001092632330.png)

Le reti **enterprise networks**, ovvero le reti aziendali sono delle reti leggermente diverse. Di solito si ha un collegamento diretto con l’ISP, attraverso il router gateway, che si occupa di dividere dentro e fuori dalla rete. Un altra serie di router permettono di portare internet ovunque, compreso nel datacenter. SI connettono agli switch anche gli access-point wifi, oppure i laptop e le prese ethernet.

Come effetto finale si ottiene che nella rete aziendale ci si collega come fossimo a casa, quindi o con l’ethernet o il wifi.



Le **data center networks** si hanno degli accessi di solito ridondanti e con molti Gbps di banda di accesso, siccome si deve dividere questo collegamento in molte sezioni. Queste reti servono a collegare centinaia di server tra di loro e alla rete. Di solito questi centri hanno come collo di bottiglia la linea di accesso alla rete, per questo di solito si studia dove posizionarli in base alla posizione di accesso sulla rete, che deve essere vicina a più ISP con alta banda. La potenzialità dei datacenter è la virtualizzazione, ovvero la capacità di avere un certo numero di risorse computazionali (anche elasticamente modificabili) e facilmente scalabile (modificare la richiesta di server in base all’effettivo utilizzo dell’applicaizone).



#### Mezzi trasmissivi

Per trasportare i dati su una rete, sono necessari dei segnali diversi che cambiano anche in base alla tecnologia a cui si appoggiano le connessioni.

Questi segnali possono essere **mirati**, ovvero il segnale si propaga in un mezzo fisico, come la fibra ottica o il filo di rame o cavo coassiale. Questo permette di avere un controllo più efficace sulla propagazione del segnale. 

Utilizzando dei **mezzi a onda libera** invece, i segnali si propagano nell’atmosfera e nello spazio esterno in modo tridimensionale, facendo in modo che la stessa potenza venga spalmata in un area molto più grande, e di conseguenza avendo una controllo sulla propagazione e ricezione del segnale minore.

Attraverso questi mezzi si trasmettono 

> **Bit**
>
> dato che viaggia da un terminale a un altro, passando per una serie di coppie trasmittente-ricevente.



> **Mezzo Fisico**
>
> L’effettivo tramite tra chi trasmette e chi riceve.



Il **cavo coassiale**, utilizzato prima per le televisioni, poi anche per l’ethernet. Sono due conduttori di rame concentrici, con capacità di trasmissione bidimensionale e la possibilità di avere una banda di base (canale singolo, capacità Ethernet), oppure una banda larga (con più canali su un unico cavo e possibilità di segnale video).

Ad oggi la migliore tecnologia per la comunicazione internet invece è la **fibra ottica**, costituita da un mezzo sottile e flessibile che conduce impulsi di luce. Questa linea è costituita da un alta frequenza trasmissibile da punto a punto (anche fino 10-100Gbps). Il segnale è caratterizzato da un basso tasso di errore, immune all’interferenza elettromagnetica. Necessità però di ripetitori e è molto sensibile alle piegature o danni durante i lavori. Di solito su ogni linea ci possono essere cavi contenenti anche centinaia di fibre singole.  



Un altro metodo di comunicazione abbastanza conosciuto sono attraverso i **canali radio**, che permettono di trasportare segnali sfruttando lo spettro elettromagnetico. Questo metodo di comunicazione hanno il lato positivo che non richiedono la posa fisica di cavi, di essere bidirezionali. Hanno come contro che sono soggetti agli effetti dell’ambiente, quindi sono sensibili a riflessione, ostruzione e interferenza.

Ci sono diversi tipi di canali radio:

* collegamenti a **microonde terrestri**: sono dei collegamenti veloci fino a 45Mbps. Viene utilizzato come backup su delle linee in fibra oppure per studiare una successiva creazione di fibra e temporaneamente creare il collegamento.

* internet **satellitare**: fornisce una banda di accesso intorno ai 45Mbps, il problema di questa connessione è che il ritardo di trasmissione è di circa 270-300 secondi (quando il collegamento è geostazionario). Hanno il lato positivo che possono servire un intera metà del pianeta con un unico satellite (sempre geostazionario).

  Per risolvere questo problema si può o fornire un collegamento a più bassa quota, ma per fare questo è necessaria una costellazione di satelliti oppure la connessione non può essere continua.

  Il vantaggio di questo metodo è che può portare abbastanza semplicemente internet in ogni posizione del mondo.

  I costi al momento sono ancora molto alti.

* **LAN** (es. Wifi), fornisce una rete di accesso di circa 11-54-300+Mbps. 

* **Wide-Area** (es: rete cellulare) fornisce un collegamento fino a 100Mbps (Rete 4G-LTE), in continuo sviluppo.



Spesso se si fa un test per controllare la velocità del collegamento, ad esempio con *speedtest.com*, si ottiene una velocità decisamente più lenta di quelle esposte in questi schemi. Questo dipende poi dal collo di bottiglia e è quello che mi rallenta le prestazioni.



### 1.2.2 Nucleo della rete

Nonostante i confini della rete siano abbastanza diversificati fra di loro, la parte interna della rete è molto più simile e costante. 

Di solito più si aggrega traffico, più la rete è “magliato”, ovvero sono presenti router che permettono la connessione.

Ci sono due tecnologie che permettono il passaggio di dati in questa architettura, basati entrambi con la **commutazione** ovvero l’azione di prendere in ingresso i dati da una porta e redirezionarli su un altra, per indirizzare il traffico e mandare i pacchetti dove effettivamente devono andare. Queste tecnologie servono a capire come smaltire il traffico della rete.

Ci sono due tecnologie per fare queste azioni, commutazione di circuito e commutazione di pacchetto.

Questi due metodi si basano su due concetti diversi: la commutazione di circuito si basa sul fatto che ogni circuito ha una data banda e una serie di altre informazioni che conosco. La commutazione di pacchetto invece lascia la libertà di come i dati vengano mandati, e di conseguenza rende adattabile la rete a tutti i tipi di dati che la popolano.

Logicamente queste due tecnologie non possono convivere nella stessa rete.

#### commutazione di circuito: 

tecnologia nata con la rete telefonica. Si creava un circuito tra ricevente e trasmittente e si pagava una quota al minuto perché in quel momento si teneva impegnata la rete. Questo concetto funzionava bene perché questa rete serviva solo per lavorare con le frequenze vocali.

Questo tipo di comunicazione fornisce un collegamento che impiega qualche secondo a essere creato, ma che rimane costante e invariato fino a che non si termina.

Di solito questa tecnologia si utilizza solamente per la voce. 

Questo metodo inoltre riserva il circuito finché non si termina la chiamata. Questo concetto di fatto impone uno spreco di dati nel caso non si stesse completamente occupando la linea. 

#### commutazione di pacchetto:

 è una tecnologia molto diversa nata per la comunicazione digitale, dove i bit possono essere mandati in gruppo o meno e sono sempre caratterizzati dal *non* essere continui (al contrario della voce). Per questo motivo è stata sviluppata questa soluzione. I dati in questa tecnologia fa in modo da incapsulare i dati nei pacchetti e tutti gli host non hanno limiti su quanti pacchetti inviare o ricevere. Questa rete tratta ogni pacchetto in modo uguale, di fatto quindi senza controllo sui flussi di pacchetti in entrata e in uscita. In teoria si autobilancia fornendo più spazio per mandare i pacchetti di chi ne sta mandando di più.

Questo modello permette di avere una velocità si adatta alle necessità degli host che la stanno usando. Di fatto questo significa che la rete è molto performante se si è in pochi ad utilizzarla, meno performante quando tanti condividono lo stesso link. 

Questo metodo fornisce il lato positivo di avere una rete che non rifiuta mai una connessione e che quindi permette sempre di mandare un pacchetto, ma può accadere che una parte della rete non riesca a smaltire più il traffico se gli host ne creano troppo e quindi la rete si blocca.

Questa rete ha la caratteristica che necessita di meno cavi e permette di dividere una banda molto grande in capacità in piccoli “pezzi”. Queste tecniche si chiamano di **suddivisione del canale** (suddivisione dell’ampiezza di banda, *bandwidth*). Per trasmettere posso dividere nella frequenza e fare una connessione continua nel tempo utilizzando diverse frequenze. Nel caso invece io potessi dividere solo nel tempo potrei fornire una certa quantità di tempo a ogni pacchetto e in questo modo fornire a rotazione la possibilità di trasmettere. Quest’ultima tecnica può essere fatta solo con dati numerici, la voce per definizione continua non può essere trattata in questo modo.

(lezione 4)

Per la suddivisione del canale si hanno due possibili tecniche (spiegate sopra), chiamate **FDM (Frequency Division Multiplexing)** e **TDM (Time Division Multiplexing)**. 

In ogni caso la FDM viene utilizzata sempre nelle tecniche di comunicazione wireless e nelle chiamate telefoniche. Il TDM viene utilizzata ogni canale ogni spot di tempo e è utilizzabile con la commutazione di pacchetto. In questo caso tutto il flusso viene diviso in pacchetti e vengono messi in seguito per condividere le risorse di rete. Ogni volta che un pacchetto viene mandato utilizza completamente la banda del collegamento. Per questo motivo nella rete si crea la **contesa per le risorse**, dove i pacchetti aspettano di avere lo spazio per avere tutta la banda disponibile per essere mandato. In questo modo però si crea il problema della **congestione**, ovvero quando la pila di richieste diventa troppo lunga per eseguire le operazioni di commutazione in tempi rapidi e di conseguenza l’attesa per l’utilizzo del collegamento continua. Se la congestione peggiora al punto da riempire il buffer della coda del router, allora non c’è più spazio per ricevere pacchetti e quindi si parla di **congestione grave** e si verificano perdite di pacchetti oppure blocco totale della comunicazione lungo questo nodo. 

Per ovviare a questo problema di solito il router identifica una rete di riserva meno congestionata e reindirizza i pacchetti attraverso un altra rete che porta i pacchetti allo stesso obiettivo per un altro percorso.

Ogni pacchetto segue la tecnica del **store and forward** ovvero il nodo deve ricevere completamente il pacchetto prima di poterlo reindirizzare. La memoria del router non è illimitata perché ha la necessità di avere una memoria più veloce possibile e di conseguenza non può essere troppo grande.

La congestione non è semplicemente eliminabile, siccome la rete non ha una modalità interna di autoregolarsi, ma ci sono alcune tecniche per limitare questi problemi o quantomeno aggirarle.

Il comportamento della rete a pacchetto è piuttosto complesso e prende il nome di **multiplexing statistico**. Questo metodo permette la divisione dello spazio diviso nella banda in modo che ogni host abbia una velocità finale di invio dei pacchetti più o meno costante. In questo modo l’host che invia più traffico è anche quello che ottiene più spot per mandare i suoi pacchetti. Di fatto quindi la velocità finale dei vari host per ottenere/mandare i suoi pacchetti è più o meno costante. 

Inoltre di solito i collegamenti non vanno sempre in capacità crescente, ma ogni sezione si basa sul concetto di avere solo la capacità media che passa in quel tratto di percorso. Questo permette di avere il massimo della resa nei periodi “normali” di traffico e di avere una rete leggermente congestionata nei rari momenti in cui tutti gli host sono collegati, ma un costo di realizzazione dell’opera non esageratamente grande.



Per calcolare il tempo di un percorso di store-and-forward si calcola che:

* occorrono L/R secondi per trasmette (push out) un pacchetto di `L` bit su un collegamento in uscita da `R` bps. 
* L’intero pacchetto deve arrivare completamente al router per essere ritrasmesso al successivo
* il *ritardo* quindi si calcola semplicemente come `3L/R` (supponendo che il ritardo di propagazione sia nullo)

Di fatto ci sono degli accorgimenti pensati per rendere la rete stabile, ad esempio il controllo di congestione in entrata (quando l’interfaccia di rete stessa si rifiuta di mandare un pacchetto, ritenendo la rete molto congestionata, oppure uno sharper allimboccatura, che fa cadere tutte le connessioni che ritiene avessero troppe risorse).



#### Commutazione di pacchetto e commutazione di circuito

La commutazione di pacchetto è di fatto diventata la scelta vincente perché anche la più “comoda”, siccome fornisce un ottima elasticità sui dati che possono essere mandati. Questo metodo a pacchetti è ottimo per i dati a raffica, dove le risorse sono condivise. Inoltre sono necessarie pochissime impostazioni di chiamata. 

L’eccessiva congestione di pacchetti può portare alla perdita di pacchetti o a un ritardo. Per questo sono necessari protocolli per il trasferimento affidabile dei dati e per il controllo della congestione.

Si può anche ottenere un circuito *circuit-like*, ovvero che imita la commutazione di circuito attraverso una rete a commutazione di pacchetti, ma ci sono molto problemi e sono tecniche molto avanzate, non viste in questo corso di base.  



### 1.2.3 Struttura della rete Internet

Come si fa quindi a connettere tutte le reti di accesso tra loro? Ci sono degli ISP che più grandi che servono degli ISP più sempre più piccoli, fino ad avere un servizio capillare e globale. Per costruire la rete internet questa rete di ISP ha la necessità di **IXP(Internet Exchange Point)**, ovvero il luogo dove gli ISP si collegano fra loro. La presenza di questo IXP non è casuale, ma dipende dagli accordi commerciali tra gli ISP. Di solito queste connessioni sono multiple e con delle linee di backup. 

Inoltre si può pensare a ISP regionali, nazionali e internazionali. Tutti questi si servono a vicenda per fornire la rete di accesso. 

Allo stesso modo i grandi content provider che necessitano di una grande rete di distribuzione del loro servizio (es. Google, Microsoft, Akami, Amazon, ..) possono sviluppare una loro rete per potersi collegare ai nodi più grandi e meglio posizionati. Questa rete può essere fisica, quindi costruita ad hoc (es. Google Fiber), oppure possono costruire una rete di overlay, ovvero gestiscono una rete virtuale che si appoggia ad un ISP, sfruttando il servizio “affittato” come soluzione della loro rete.

Di fatto quindi, nella rete posso trovare:

<img src="image/image-20211002092707567.png" alt="image-20211002092707567" style="zoom:50%;" />

Al “centro” un piccolo numero di grandi reti con “molte” interconnessioni:

* **”tier-1” commercial ISPs** che offrono copertura nazionale e internazionale (es. Level 3, Sprint, AT&T, NTT)

* **Reti dei content provider**: reti private che connettono i propri data center a internet , spesso by-passando ISP tier-1 e regionali, e collegandosi direttamente agli ISP finali. Questo serve perché i contenuti in questo modo sono più vicini, e quindi ha by-passato una serie di colli di bottiglia che possono rallentare il servizio. Inoltre in questo modo si possono creare dei nodi di distribuzione per i servizi che devono essere più accessibili, in modo da velocizzare la qualità del servizio. Spesso queste reti utilizzano la creazione di reti fisiche proprie o reti overlay per avvicinare i dati all’utente finale. 

  (lezione 5)

  Molti content provider hanno la propria rete di delivery, spesso fatta in overlay con accordi con i singoli ISP. 



I nodi nella rete aumentano più si è vicini all’utente finale, mentre al cuore della rete si hanno pochi ma grandi collegamenti. Altra differenza si ha nel consumo energetico di internet: l’utilizzo energetico di un oggetto dipende dalla sua capacità di switchare il traffico. Un router che consuma di più di solito riesce a smistare più pacchetti o più velocemente.

I collegamenti intercontinentali solitamente sono fatti con cavi in fibra ottica, e è interessante pensare che sfruttino le vecchie rotte di navigazione transoceanica del 500. ![https://submarine-cable-map-2015.telegeography.com/]

Altre informazioni sulla struttura della rete è la struttura della *NSFnet*, ovvero la rete degli istituti di ricerca americani.

In Europa abbiamo la rete **GEANT** per la ricerca, ma a livello nazionale. Questa rete quindi permette di collegare i punti di collegamento fondamentale internazionale. 

<img src="image/image-20211002101942846.png" alt="image-20211002101942846" style="zoom: 33%;" />

Quando questo collegamento arriva in Italia (a Milano) si crea la rete del consorzio **GARR**, ovvero l’ISP della ricerca, che collega gli istituti di ricerca in Italia e poi si innesta come collegamento per gli stati confinanti. Di solito questo consorzio affitta una “dark-fiber”, ovvero una rete non utilizzata e viene collegata alla rete attraverso dei router fornito dal consorzio GARR. 

Questa rete fornisce un accesso a internet, ma per avere questo collegamento devo poi uscire da questi percorsi.

<img src="image/image-20211002102132527.png" alt="image-20211002102132527" style="zoom:33%;" />

## 1.3 Perdite e ritardi

Sulla rete internet a commutazione di pacchetto, ognuno viene gestito singolarmente e si elaborano seguendo le direttive del store-and-forward fino anche non arrivano a destinazione. Di fatto quindi stanno nel buffer di un router finché non arrivano a destinazione.

Quando i pacchetti arrivano nel buffer, si accodano agli altri pacchetti, in attesa al proprio turno. Di fatto quindi il ritardo può avvenire nel buffer (mentre i pacchetti sono in attesa di essere trasmessi), in accodamento(aspettando che tutto il pacchetto venga ricevuto) oppure può avvenire una perdita di pacchetto se questo arriva e trova un buffer pieno.

Di solito si possono identificare 4 principali cause di ritardo del pacchetto:

1. Ritardo di **elaborazione del nodo**: questa fase avviene nel router o nel nodo di scambio e serve per eseguire una serie di controlli sul pacchetto, ovvero il controllo di errori sui bit (controllo della correttezza del pacchetto) e la determinazione del canale di uscita.

   Questo tempo è da considerabile costante, siccome i router commerciali sono ottimizzati al massimo in queste operazioni.

2. Ritardo di **accodamento**: quando il pacchetto viene smistato, si mette in attesa di trasmissione nel buffer.

   Questo contenuto è il più difficile da calcolare perché dipende dagli altri pacchetti, dal funzionamento della rete in generale e anche come è congestionata la linea in utilizzo. Di solito si stima questo valore in un range elaborato solo tramite valori statistici.

3. Ritardo di **trasmissione**: quando il pacchetto finisce la coda, allora viene trasmesso. Questo è il tempo che l’interfaccia di rete impiega a creare la forma d’onda necessaria per il trasporto del mio pacchetto, ovvero quanto tempo impiega il segnale nel quale è stato tradotto il tuo pacchetto a essere completamente eseguito. 

   Questo è un valore facilmente quantificabile, infatti sapendo quanto il segnale di un certo valore di bit impiega a essere trasmesso, allora si può calcolare per qualsiasi valore di bit. Serve quindi la *lunghezza del pacchetto* $L$, la *frequenza di trasmissione del collegamento (in bps)* $R$, allora il ritardo di trasmissione è $L/R$.

4. Ritardo di **propagazione**: si calcola semplicemente facendo $d = \text{lunghezza del collegamento fisico}$ e $s = \text{velocità di propagazione del collegamento}$, si ottiene il ritardo di propagazione con $d/s$. 

   Questo calcolo non valuta, come si vede, la dimensione del pacchetto e la frequenza di trasmissione, che invece vengono calcolati nel ritardo di trasmissione.

   Di solito queste indicazioni si trovano specificatamente su ogni mezzo utilizzato, e con questa costante avendo le indicazioni della lunghezza del collegamento allora si ottiene questo valore di ritardo.

   Questo tempo viene calcolato per una tratta, e rimane costante per ogni pacchetto che viene inviato attraverso quel nodo. Questo valore può variare in casi particolari, ad esempio con una comunicazione satellitare, dove il ricevitore non è fermo in orbita geostazionaria e quindi la distanza (e di conseguenza il percoso) cambiano.



Il **ritardo di nodo** quindi si può semplificare con questa formula $d_{nodal} = d_{proc} + d_{queue} + d_{trans} + d_{prop}$:

* $d_{proc}$ ovvero ritardo di elaborazione (*processing delay*).

  Di solito pochi microsecondi, anche meno

* $d_{queue}$ ovvero ritardo di accodamento (*queuing delay*)

  Dipende dalla congestione della rete. Si può stimare attraverso la teoria delle code: analizzando il tempo di attesa di un oggetto nel buffer, si ottiene un grafico di questo tipo

  <img src="image/image-20211004110215996.png" alt="image-20211004110215996" style="zoom:67%;" />

  Dove si identificano $R = \text{frequenza di trasmissione in bps}$, $L= \text{lunghezza del pacchetto in bit}$ e $a = tasso medio di arrivo dei pacchetti$. $La/R$ è definito come *intensità di traffico*. Con questo fattore tendente a zero, il ritardo è pressoché nullo, se si arriva a 1 il ritardo si fa consistente, ma se questo valore diventa maggiore di 1 vuol dire che il lavoro in arrivo è più di quando possa essere svolto, quindi il ritardo medio può tendere all’infinito. 

  Questo calcolo significa che progettando la rete si deve far in modo da non avere mai un canale completamente saturo, perché così facendo si ottiene una rete più lenta.

  Di fatto nella realtà, questo ritardo viene avvertito solo in caso di congestione, in caso contrario è inavvertibile o inesistente (se la rete è scarica).

* $d_{trans}$ ovvero ritardo di trasmissione (*transmission delay*)

  Varia con valore $L/R$ e è significativo solo sui collegamenti a bassa velocità.

* $d_{prop}$ ovvero ritardo di propagazione (*propagation delay*)

  Può variare da un valore di pochi microsecondi a centinaia di millisecondi. 



Per avere una diagnostica su questi dati si utilizza un programma chiamato `traceroute`. Questo programma crea dei pacchetti che hanno un parametro, chiamato *tempo di vita*, ordinati in modo crescente. Questo serve per avere l’eliminazione di questi pacchetti in un certo router. Quando viene effettuata questa eliminazione, il nodo risponde al terminale che è stato eliminato questo pacchetto. Facendo così si ottengono le informazioni sui nodi del percorso e anche le informazioni sulla velocità del collegamento.

Un esempio di questo comando può essere così:

```bash
su un massimo di 30 punti di passaggio:

  1     4 ms     2 ms     1 ms  10.196.160.1
  2     *        *        *     Richiesta scaduta.
  3     *        *        *     Richiesta scaduta.
  4     4 ms     5 ms     2 ms  193.205.210.253
  5     5 ms     2 ms     3 ms  ru-unitn-povo-re1-tn1.tn1.garr.net [193.206.143.77]
  6    23 ms     6 ms     9 ms  re1-tn1-rx1-mi2.mi2.garr.net [90.147.81.113]
  7     9 ms     6 ms     8 ms  garr.mx1.mil2.it.geant.net [62.40.125.180]
  8    14 ms    14 ms    15 ms  ae6.mx1.gen.ch.geant.net [62.40.98.80]
  9    23 ms    22 ms    21 ms  ae6.mx1.par.fr.geant.net [62.40.98.183]
 10    98 ms    94 ms    92 ms  hundredge-0-0-0-22.102.core1.newy32aoa.net.internet2.edu [198.71.45.236]
 11   100 ms    93 ms    92 ms  163.253.1.43
 12    97 ms    99 ms    97 ms  nox300gw1-i2-re.nox.org [192.5.89.221]
 13   100 ms    98 ms    98 ms  192.5.89.58
 14    99 ms    98 ms    99 ms  nox-mghpcc-gw1-umassnet-re2.nox.org [18.2.8.90]
 15   102 ms    97 ms    97 ms  69.16.1.0
 16    99 ms    98 ms    99 ms  core2-rt-et-8-3-0.gw.umass.edu [192.80.83.113]
 17   101 ms    98 ms   100 ms  n5-rt-1-1-et-10-0-0.gw.umass.edu [128.119.0.10]
 18   101 ms    98 ms    99 ms  cics-rt-xe-0-0-0.gw.umass.edu [128.119.3.32]
 19   103 ms   124 ms   101 ms  nscs1bbs1.cs.umass.edu [128.119.240.253]
 20    99 ms    97 ms    98 ms  gaia.cs.umass.edu [128.119.245.12]

Traccia completata.
```

Di importante in questa traccia è il passaggio da nodo 9 al 10, dove avviene un drastico aumento del tempo di passaggio. Questo indica ad esempio (in questo caso, non sempre) che è stato fatta una connessione transoceanica.

Un altro particolare è il risultato `* * *`. Questo avviene (ormai) spesso perché alcune reti per sicurezza non lasciano “vedere” dentro, oppure hanno configurato i gateway o i firewall in modo da “droppare” i pacchetti nel caso di un `traceroute`.

(lezione 6)

I pacchetti vengono persi di solito perché una coda ha capacità finita, e il pacchetto non era memorizzabile nel buffer, ovvero per *congestione*. Quando il buffer è completamente pieno allora si va in **congestione grave**. Questi pacchetti vengono persi, ma non si occupa la rete di recuperare i pacchetti persi, ma l’host.

Siccome la rete è l’unica che conosce il suo stato, ma gli host hanno il compito di mandare o rimandare i pacchetti in caso vengano persi, allora c’è bisogno di una sottile modalità di gestione per fare in modo che non si intasi la rete, mentre cerco di mandare dei pacchetti mentre la rete è congestionata. 

La rete è delicata, infatti c’è un bilanciamento tra affidabilità (ridurre la possibilità di dati errati e la capacità di recuperarli) e la sostenibilità della rete (mantenere il collegamento funzionale al meglio).

La rete moderna di solito non ha problemi seri in un tempo continuato, ma di solito ha un picco di connessioni e poi il traffico viene smaltito.



## 1.4 Throughput: velocità della rete

Questo parametro valuta gli effettivi bit/byte per unità di tempo che si possono ricevere/mandare da mittente o ricevente. Si può calcolare il valore **istantaneo** e il valore **medio**.

Il throughput è istantaneo se si calcola quello di un valore predefinito, oppure il valore legato a un singolo pacchetto. Di solito per questo motivo si utilizza il valore medio. 

Questo valore può dipendere anche dalla quantità di utenti che hanno un accesso su un determinato collegamento. Inoltre se la rete è condivisa, e non dedicata, allora bisogna calcolare che non si può utilizzare tutta la banda di quella linea.

Questo valore è importante in modo da segnalare a delle applicazioni se il loro collegamento è fattibile, in base alla richiesta di velocità della rete per il funzionamento. 

Si possono fare delle assunzioni abbastanza significative attraverso il concetto di **collo di bottiglia**: la prestazione che si ottiene nel collegamento con capacità più piccola si ottiene calcolando il minimo tra le capacità dei collegamenti che creano il mio percorso. Questo significa che il rallentamento peggiore è quello che alla fine regola la velocità del trasporto.

Viene fatto un calcolo di questo tipo per capire che prestazione fornisce una rete, in base a quanto io abbia bisogno di rete in un determinato momento. Di fatto dopo un analisi di questo tipo si possono spostare o modificare delle connessioni sulla rete per migliorare questo blocco o by-passarlo. 

Le grandi aziende utilizzano questo valore per identificare le zone migliori dove posizionare i data-center.

Per migliorare il throughput si possono pensare a vie diverse per raggiungere quella posizione, oppure portare un miglioramento alla linea di contatto peggiore.

Es:

<img src="image/image-20211004113912899.png" alt="image-20211004113912899" style="zoom:67%;" />

Il throughput si calcola trovando il minimo tra $R_c, R_s, R/6$.



## 1.5 Livelli di protocollo

Le reti sono molto complesse e comprendono un sacco di device, per questo motivo dagli anni 70 hanno iniziato a introdurre una tassonomia della rete. 

Questa organizzazione della rete si basa su una serie di operazioni sequenziali che hanno lo scopo di fornire valori alla prossima operazione. Tutte queste azioni in ordine un pochino più furbo. 

Queste operazioni si identificano come dei livelli, e ognuno di questi può parlare solo con il precedente o il successivo.

Inoltre questo modo di dividere le operazioni serve anche per semplificare quello che, nel suo complesso, sarebbe un problema troppo grande e troppo specifico da risolvere. In questo modo, come l’architettura dei computer, si ottiene un modello che fornisce una grandissima elasticità di applicazioni senza perdere nessuna parte del percorso.

In alcuni casi però la generalizzazione è un lato negativo. Ad esempio nel caso io voglia un sistema completamente off-shore, con la necessità di avere la batteria più ottimizzata possibile e è necessario avere dei vincoli prestazionali non indifferenti, questa struttura non è la soluzione migliore. Di solito in questi casi si tende a sviluppare una tecnologia apposita per garantire questi dati, quindi di fatto questa struttura per un elemento del genere non fornisce un aiuto, ma crea più un problema che altro. (Ci sono degli elementi in cui questo modello a strati viene “schiacciato” per garantire i vincoli prestazionali).



## 1.6 Pila di protocolli internet: Modello TCP/IP

Questa astrazione è la pila responsabile per il funzionamento di internet.

A ogni livello appartiene una funzionalità particolare specifica, in modo che ognuno abbia il suo compito principale. Di solito l’utente o il software che utilizza la rete si appoggia a questa pila, ovvero entra in contatto con l’astrazione della rete. 

L’accesso alle funzioni di questa pila sono offerti all’interno di ogni host e sono responsabili di fornire le funzionalità di base dei servizi per collegarsi a internet. Le parti di questa pila sono direttamente inserite nel sistema operativo, non entrano mai in contatto diretto con l’utente finale. 

Ogni livello di questa pila si può interfacciare **solo** con i livelli successivi o precedenti. Inoltre, lasciando questa struttura così generalizzata, si possono aggiungere o rimuovere eventuali altri livelli per aggiungere funzionalità o altro. 

La **pila TCP/IP** è costruita in questo modo:

1. **fisico**: questo livello è responsabile di convertire la sequenza di bit creata nel link come una frequenza d’onda che si può propagare. Esempio: la luce della fibra che segnala 1 se luce e 0 se non luce.

   Questa sezione è importante soprattutto perché ci sono delle teorie per trasportare più dati nella stessa frequenza d’onda utilizzando delle tecniche sviluppate e molto avanzate.

2. **link (collegamento)**: instradamento dei pacchetti attraverso la serie di commutatori di pacchetto. Questo permette di avere un percorso fatto anche con tecnologie diverse, e si può utilizzare attraverso l’instradamento di pacchetto. Di fatto questo livello gestisce il collegamento da un nodo all’altro.

3. **rete **:instradamento dei datagrammi dall’origine al destinatario, ovvero crea il collegamento per il livello di trasporto, creando la strada svolgendo una ricerca tramite grafo e di fatto creando la “strada” da sorgente a destinatario. 

4. **trasporto**: il livello di trasporto costruisce quella connessione virtuale che mi permette di vedere direttamente il server, in cui non si percepisce la rete, ma si ottiene semplicemente il contatto che viene tipo “oscurato” come fosse un unico tunnel. 

   Costruisce uno o più percorsi attraverso la rete per raggiungere l’host cercato. Questo layer lavora con il più basso layer di rete per fornire la connessione. (fornisce il collegamento host-to-host)

5. **applicazione**: supporta l’applicazione di rete, ovvero applicazioni che si interfacciano con la rete. Questo livello mi permette di avere un linguaggio univoco comprensibile a ogni macchina. 

   Il sistema operativo fornisce questa interfaccia per comunicare con la rete a alto livello, ma è anche l’unico layer che si può bypassare, interagendo direttamente con il layer di trasporto.

   Esempi di applicazioni di rete sono i numerosi protocolli, ad esempio: FTP, SMTP, HTTP, HTTPS, ...

![image-20211004181929759](image/image-20211004181929759.png)

Questi livelli sono tutti necessari per la comunicazione e siccome ogni livello si interfaccia solo con il layer sopra o quello sotto, è necessario un approccio top-down-bottom-up per la comunicazione tra due host (per garantire **servizio, stratificazione e incapsulamento**).

Ogni livello non fa altro che prendere i dati ottenuto dal layer superiore e icapsularli in un pacchetto creato in modo che quando questi dati verranno forniti allo stesso livello dell’altro host, allora il primo è in grado di comunicare in maniera univoca con il secondo. L’intestazione fornita dal livello di trasporto, chiamata *header* viene creato e “aggiunto” al pacchetto.

 Il messaggio dello strato applicazione insieme all’header di trasporto fornito nello strato trasporto si chiama **segmento** di trasporto.  

Il protocollo di rete trasferisce il segmento da un host all’altro trovando il percorso. Il tipo di dati che vengono scambiati però viene chiamato **datagramma di rete**. Questo datagramma ha due intestazioni: una esterna che è a livello di rete, e una interna del livello di trasporto (ma questa non è importante per il suo scopo).

Il protocollo di link incapsula il datagramma di rete con l’header di link H per creare un **frame/trama**, responsabile poi di viaggiare verso la destinazione.

![image-20210928144728616](image/image-20210928144728616.png)

Una volta che il secondo host riceve questo frame, allora i dati ricevuti risalgono la pila protocollare (vengono “spacchettati”), ovvero vengono **de-incapsulati**. Ogni strato toglie il suo “strato” di dati e così facendo  fa i suoi controlli e passa la mole di dati al livello successivo.

Nonostante l’incapsulamento, ogni layer omologo ha l’impressione di parlare sullo stesso livello. Questi livelli riescono con questa tecnica a operare la comunicazione orizzontale, e quindi a comprendersi a vicenda.



Questa stratificazione a 5 livelli sono implementati in **ogni host** e funzionano in serie, uno sopra condivide con il livello sotto o il livello sopra. Una volta che si incapsula completament, per recuperare il messaggio serve de-incapsulare completamente.



Questa pila viene chiamata **pila TCP/IP**, e è quella utilizzata normalmente.



## 1.7 Modelli di riferimento ISO/OSI

Questo è un modello di standardardizzato all’incirca nello stesso periodo del modello TCP/IP. Questo modello fornisce due layer in più, dando alcune informazioni e utilizzi utili ma non necessarie a tutti per lo stato di applicazione.

Nella pratica questo modello non è utilizzato, nonostante questo sia l’unico protocollato e studiato. Il modello TCP/IP è il modello *de facto*.

<img src="image/image-20211004183831775.png" alt="image-20211004183831775" style="zoom:50%;" />



## 1.8 Comunicazione tra host non direttamente collegati

Questo è come la comunicazione avviene tra due host direttamente collegati, ma come funziona il collegamento tra due host non direttamente collegati tra loro? 

<img src="image/image-20210928145303344.png" alt="image-20210928145303344" style="zoom:50%;" />

Gli host sono gli unici che hanno tutti e 5 i livelli. Gli switch e i router di rete non hanno tutta la pila, perché non hanno la necessità di “spacchettare” tutti i livelli del messaggio ma lavorano solo a livello 3, quindi instradando il traffico attraverso il minor numero di informazioni necessarie. Il fatto che il router non abbia il livello 4 e il 5 è dimostrazione del fatto che non deve interferire nei dati effettivamente comunicati. Se un router svolge questa azione non è più definibile router (non nel senso tradizionale almeno).

Inoltre sul router per costruzione non ha applicazioni a cui fornisce supporto, motivo per cui non gli serve tutta la pila TCP/IP.

Lo switch può ricevere fino a livello 2, motivo per cui viene anche chiamato commutatore.



## 1.9 Sicurezza di Rete

Quando sono strati creati i primi protocolli le persone collegate non erano in pensiero per la sicurezza, perché gli unici che la utilizzavano erano quelli a cui serviva e era impensabile che queste persone la attaccassero.

Questo punto di vista tenuto durante l’ideazione della rete internet ha poi dato problemi quando, diventando pubblica, c’è stato sempre più la necessità di confidenzialità delle informazioni, protezione della privacy o controlli sulla modifica del pacchetto.

Per mettere qualche soluzione a questi problemi sono state predisposte delle “patch”, ovvero delle sezioni di codice modificate per aggiungere queste funzionalità e risolvere in parte questi problemi.

Nonostante questo la sicurezza rimane un problema, perché ogni modifica permette la possibilità di errore nel codice.

I problemi più comuni sono:

* Malware che può raggiungere host in modo nascosto e aprono la comunicazione per controllare il computer obiettivo (possono essere virus, worm o cavalli di troia)

  Sempre più utilizzato per spionaggio con il fine di memorizzare le informazioni del computer e mantenute sul computer (questi modelli cercano di essere il più passivi possibili, per non essere scoperti).

* L’attacco più comune sulla rete, che sfrutta la rete è il DOS (Denial of Services), che punta a bloccare un server “bombardardolo” di richieste. Questo attacco è facilmente risolvibile attraverso firewall più controllati e black-list degli ip. 

  Più complicato invece sono gli attacchi DDoS(Distruibuited Denial of Service). Questi attacchi creano una rete di “bot”, ovvero computer infettati con uno specifico virus, che viene attivato/si attiva per fare richieste verosimili al server. Logicamente in questo caso è più difficile porre rimedio, perché si parla di milioni di IP e non si riesce a distinguere il normale utilizzo o invece l’effettivo attacco da parte di ogni host collegato.

  Spesso questi gruppi di bot vengono raggruppate in una vera e propria rete virtuale chiamata *botnet*.

* Il malware è spesso autoreplicante, ovvero un host infettato può passare lo stesso virus a altri host sulla stessa rete.

  <img src="image/image-20211004185549353.png" alt="image-20211004185549353" style="zoom:50%;" />

* Packet sniffing: si tratta di una tecnica per intercettare i pacchetti sfruttando il fatto che in una rete wifi tutti (potenzialmente) sono ascoltati da ogni host sulla rete, e viene accolto solo se ci è direttamente destinato (questa tecnica di comunicazione “in comune” si chiama broadcast). 

  Di solito queste tecniche sono molto utili soprattuti nella fase di reconassance, per capire che tipo di tecnologie utilizzate e eventualmente dei dati comunicati in chiaro.

  Ad oggi ogni connessione viene “spinta” per essere privata, adottando il protocollo **https** che fornisce un layer di sicurezza solitamente abbastanza buono per proteggersi da una scansione casuale.

  Questo attacco permette il *packet spoofing* ovvero l’imitazione di una serie di pacchetti con il fine di garantire l’accesso.

Di fatto molti software utilizzati in questi attacchi diventano vettori di attacco solo se utilizzati per questi scopi, quindi tutto diventa cattivo se lo si utilizza per gli scopi sbagliati.



## 1.10 Storia di Internet

* $1961$: Kleinrock nella sua tesi di laurea dimostra l’efficacia delle reti a commutazione di pacchetto rispetto a quelle a commutazione di circuito

* $1964$: Baran, visti questi studi, propone l’utilizzo delle tecniche di commutazione di pacchetto nelle reti militari, siccome erano più sicure e era tempo di guerra fredda.

* $1967$: Nasce ARPAnet, sviluppato dalla ricerca avanzata per l’esercito statunitense (ARPA: Advance Research Project Agency). L’applicazione doveva servire per un login remoto.

* $1969$: il primo nodo operativo di ARPAnet viene creato

* $1972$: viene fatta la dimostrazione pubblica di ARPAnet, con un primo protocollo elementare per gestire il traffico tra i nodi e il primo programma di *posta elettronica* (programma che diventerà predominante nella rete per qualche tempo). ARPAnet cresce fino a 15 nodi.

* **Iniziano a nascere le prime reti proprietarie**

* $1970$: viene creata la prima connessione internet satellitare chiamata ALOHAnet per collegare le università delle Hawaii.

* $1974$: Cerf e Kahn creano l’architettura per l’interconnessione delle reti, utilizzando una serie di basilari concetti come:

  * Per avere una rete più aperta possibile, si accetta di avere una rete *best-effort*, ovvero cerca di erogare un servizio al meglio possibile in base alle loro capacità. 
  * Tutti i collegamenti vengono fatti in modo da lasciare ogni nodo il più autonomo e minimale più possibile, in modo da evitare una serie di tutti i collegamenti interni. 
  * L’introduzione nella rete del router *stateless* ovvero che non ricordano i pacchetti che ci sono passati attraverso, fornendo un controllo decentralizzato.
  * questi principi definiranno poi l’architettura della rete che tuttora abbiamo.

* $1976$: Ethernet allo Xerox PARC

* con la fine degli anni ‘70 si elaborano una serie di architetture proprietarie per il funzionamento della rete.

* $1979$: ARPAnet festeggia i 200 nodi.

* **Vengono elaborati nuovi protocolli, modelli e le reti prolificano**

* $1983$: rilascio del controllo protocollare della pila TCP/IP (protocolli di rete tutt’ora validi).

* $1982$: creazione del protocollo `SMTP` per la posta elettronica (anche questo tuttora utilizzato).

* $1983$: definizione del DNS per la traduzione degli indirizzi IP.

* $1985$: definizione del protocollo `FTP`

* $1988$: introduzione del controllo di congestione TCP.

  Questa operazione viene svolta dando agli host il controllo sullo stato della rete, impedendo quindi l’immissione di troppi dati su un solo nodo. Molte di queste scelte sono fatte per mantenere la rete nella situazione più facile possibile nel core, portando tutti i servizi sugli host e ai margini della rete e quindi mantenendo l’interno molto flessibile e agile.

* Si iniziano a creare nuove reti nazionali e di ricerca. Si contano circa 100.00 hosts collegati.

* **Commercializzazione di internet, Web e nuove applicazioni**

* $\text{Primi anni '90}$: ARPAnet viene dismessa. Viene creato il **WEB**:

  * ipertestualità come modello di comunicazione internet per le pagine web
  * Berners-Lee elabora HTML e HTTP per questo scopo
  * Si creano i primi browser (al tempo solo visualizzatori web) come Netscape.

* $1991$: NSF (la rende rete di ricerca) fa cadere i diritti per l’utilizzo commerciale. Da questo punto poi si iniziano a creare una serie di protocolli per la comunicazione web e interazione client-server (protocollo http) e la commercializzazione della rete internet e delle applicazioni a essa collegata.

* $\text{fine anni ‘90 - 2007}$ Iniziano le prime applicazioni che svolgono applicazioni fortemente collegate a internet:

  * Prime applicazioni *“killer”* che implementano la messaggistica istantanea e condivisione di file peer-to-peer. 
  * Le dorsali di fibra transoceaniche vengono rafforzate e migliorate notevolmente (nell’ordine del Gbps).
  * Si inizia a parlare di sicurezza di rete
  * Si contano 50 milioni di hosts e oltre 100milioni di utenti

* $\text{Dal 2005 al tempo presente}$: Scalabilità, SDN, mobilità e Cloud:

  * $2008$: Si inizia a sviluppare il **sofware-define networking**, metodo di creare reti molto avanzato.
  * Si ha sempre più spesso una rete AAA (anyone, anytime, anywhere) con le tecnologie WIFI e 4G ad alta velocità.
  * I grandi fornitori di servizi (Google, Facebook, Microsoft) iniziano a creare le loro reti, bypassando le reti commerciali per portare i dati più vicino all’utente, evitando problemi di congestione, migliorando la qualità del servizio, avendo più banda disponibile e staccandosi dal concetto di servizio centralizzato. Si creano quindi degli **accessi** sempre più **istantanei**.
  * Le applicazioni puntano sempre più ai servizi in **cloud**: più semplici da sviluppare delle tecnologie client-server, molto più convenienti e facili da gestire, oltre che essere molto più flessibili e scalabili.
  * Iniziano a essere collegati sempre più dispositivi mobili che fissi: si punta sempre più una connessione in mobilità. Si stimano circa 18B di dispositivi collegati a internet. 
  * Gli oggetti collegati a internet hanno anche un utilizzo dei dati e un utilizzo della banda molto diverso dagli utenti normali, imponendo delle variazioni delle caratteristiche della rete e della banda di ingresso. Adesso l’internet è ovunque, con i dispositivi **IoT**.

* Dopo il 2018 c’è sempre più la tendenza ad utilizzare tecnologie che portano verso il  **cloud-edge continuum**,  ovvero l’utilizzo di una struttura cloud di base, con un metodo per fornire i dati in modo non centralizzato e quindi più vicino agli utenti finali, garantendo in questo modo un servizio migliore e più rapido. 

  Questo concetto estremizzato forse porterà nel futuro un nuovo concetto di rete basato sull’integrazione integrazione continua tra edge-cloud e CDN che avvicinano sempre più i dati all’utente finale.



# Capitolo 2: Livello applicazione

## 2.1 Principi delle applicazioni di rete

Nonostante il nome, questo livello non si occupa di applicazioni che *usano* internet per funzionare, ma quello strato che permette alle applicazioni di comunicare in modo facile e generico.

Creare applicazioni di rete significa creare programmi che girano su sistemi terminali diversi e comunicano attraverso la rete, come ad esempio le comunicazioni di un software web, oppure un applicazione di messaggistica piuttosto che *youtube*.

Per fare questo è necessario che questi programmi possano girare su più macchine differenti, in modo da garantire la continuità del servizio e soprattutto la completa compatibilità con gli altri oggetti sulla rete. Per ottenere questo risultato si è appunto creata questa astrazione generica che riesce a interfacciarsi con tutti i dispositivi possibili.

Inoltre il fatto che si sfruttano questi programmi permette che non si debba pensare a tutte le macchine nel percorso fino all’utente finale, permettendo quindi uno sviluppo più facile.

Inoltre il lato positivo di avere un sistema stratificato come il modello TCP/IP è che lo studio di un singolo livello ci consente di concentrarci solo su quello che questo livello compie, sapendo che può al massimo chiedere informazioni al livello sottostante (il livello di trasporto) o a quello successivo (il livello di rete). Il livello sottostante fornisce dei servizi esposti a questo.

Ci possono essere più **architetture delle applicazioni di rete**:

1. *client-server*

   Con questa architettura si dispone di un server sempre attivo, mentre il client si può accendere on-demand in base all’utente per richiedere degli elementi presenti sul server. 

2. *peer-to-peer*

   In questa architettura non ci sono dei server sempre attivi, ma le connessioni sono sempre tra due dispositivi alla pari, ossia che fungono da client e da server nello stesso momento entrambi. Questa rete riesce a aumentare di capacità all’aumentare del numero di nodi che ne fanno parte. 

   Viene ritenuto il metodo migliore per diffondere contenuti nel modo più rapido possibile, perché si decentralizza il luogo da cui ottenere risposta e quindi si riesce sempre ad appoggiarsi al più vicino nodo della rete. Inoltre è facilmente scalabile, siccome è necessario solamente aggiungere nodi alla rete.

   Logicamente questo tipo di rete non ha solo lati positivi, infatti c’è un problema in particolare che ne ha impedito la diffusione e la standardizzazione per tutti i servizi: la manutenzione. Per implicita costruzione di questa rete infatti tutti i nodi sono “dispersi” nella rete (questa rete è caratterizzata infatti dalla caoticità dei nodi), rendendo difficile e costosa la manutenzione. Inoltre, essendo spesso i peer (gli host che si connettono alla rete peer-to-peer) si possono anche staccare da una rete o non fornire una banda accettabilmente alta da consentire lo scambio di informazioni, rendendo quindi alcune informazioni (se non ancora rilasciate su tutta la rete) inacessibili.

   Esistono dei modi per cercare delle informazioni nella rete peer-to-peer senza indice, ma per il momento questi metodi funzionano, ma senza garanzie di prestazioni.

3. *architetture ibride* (client-server e P2P): architetture che utilizzano entrambi i tipi in funzioni differenti (di solito per ottenere il meglio di entrambe le architetture).

   Spesso è utilizzata nelle applicazioni per parti diverse della connessione o utilizzi specifici. Oppure è utile in alcune applicazioni particolari tipo la messaggistica istantanea, che utilizza un server centralizzato per conoscere gli indirizzi degli altri contatti, per poi avere una connessione P2P con loro.

4. *cloud computing*

   Questa architettura è un aggiunta al tipo client-server. 

   Questa architettura è molto scelta ad oggi per diversi motivi:

   * la applicazione di questo modello a qualsiasi applicazione è molto semplice

   * permette la decentralizzazione del servizio completamente a carico del fornitore del servizio di cloud, permettendo quindi a un semplice utilizzatore di avere un applicazione molto performante in (in un certo modo) pochissimi passi.

   * fornisce all’acquirente una potenza di calcolo facilemente scalabile, ovvero possiede una capacità computazionale on-demand. Si fornisce la potenza necessaria al minimo funzionamento, ma poi si può tranquillamente espandere nel momento in cui si ottiene troppo lavoro da dividere solo su quella macchina.

     Questa opzione di solito è mediata completamente dal data-center a cui ci si appoggia per questo servizio, lasciando all’utente finale una configurazione minima, e un ottimo servizio in caso di picchi di traffico. 

     Questa operazione viene fatta dai fornitori semplicemente vedendo il data-center come un “aggregato” di CPU e memoria, e avendo quindi la modalità per accedere a queste risorse in blocco.

   * L’insieme di tutte queste tecniche permette di avere una serie di operazioni completamente semplificate, lasciando tantissime operazioni di gestione al data-center.

   * operazioni di backup sono automatici e completamente disponibili online

   Di fatto però questa architettura ha anche alcuni lati negativi, ad esempio non posso mai sapere dove fisicamente i miei dati sono conservati, ma sono nella posizione poco specifica di “tutto” il data-center.



Di fatto esiste anche una rete interna al computer che permette al connessione di servizi all’interno del computer, ma di solito queste comunicazioni vengono svolte a basso livello dal sistema operativo, non attraverso una rete effettiva, e vengono chiamati **schemi interprocesso**. Nonostante questo si può anche impostare un servizio che parla con la macchina attraverso un interfaccia internet (ad esempio `localhost` o `127.0.0.1`).

Di solito c’è scambio di messaggi di due host diversi attraverso la rete.

Al giorno d’oggi si tende quasi sempre a fornire applicazioni client-server, ovvero una connessione creata da due host, uno responsabile di fare le richieste e l’altro a risponderle. Spesso questi servizi poi sono posizionati in un infrastruttura del cloud.



Le **socket** sono un architettura software che serve per dare un interfaccia di programmazione o comunque di accesso alla pila di rete. Di solito ci sono librerie che forniscono questa applicazione già pronta a essere istanziata.

Questa interfaccia è analoga a una porta:

* se un processo vuole inviare un messaggio lo fa inviare attraverso le sue applicazioni con il sistema operativo.
* presuppone l’esistenza di un’infrastruttura esterna che fornirà il servizio per trasportare il messaggio fino alla destinazione.

Il socket di solito fornisce semplicemente un’API che si connette direttamente con l’implementazione della pila di internet fornita dal sistema operativo.

Per fare in modo che un messaggio possa essere inviato a un altro host il mittente deve identificare il processo destinatario. Il problema è che entrambi possono avere più servizi che stanno lavorando o che hanno necessità di un connessione con un altro host. Per questo motivo l’identificativo della macchina **IP** univoco a 32 bit è sempre associato a un **numero di porta** che a sua volta è associato a un processo in esecuzione sull’host.

```bash
indirizzo=128.119.10.10 porta=80
```

Alcuni numeri di porte sono state impostate di default, ad esempio HTTP lavora sulla porta 80, i server mail sulla 25. Le prime 1000 porte sono riservate e spesso pre-assegnate a dei servizi. Il client ha la possibilità di accesso solo alle porte con numero superiore a 1000.



Ci sono dei **protocolli a livello applicazione** che servono a definire:

* i **tipi** di messaggi scambiati
* la **sintassi** dei messaggi scambiati, ovvero quali e come sono fatti i campi che sono necessari al messaggio
* **semantica** dei campi, ovvero il significato delle informazioni presenti in ogni campo
* **regole** per determinare come e quando un processo invia e risponde ai messaggi

Ci sono **protocolli di pubblico dominio** che vengono definiti (pubblicamente) nelle RFC (Request For Comments), che descrivono tutti i punti importanti di funzionamento del protocollo da un punto di vista puramente tecnico, specificando ogni minimo particolare. Questi protocolli consentono interoperabilità (essendo liberi tutti possono utilizzarli) e sono ad esempio HTTP o SMTP.

Altri tipi di protocollo possono essere **proprietari**, e si ottengono informazioni sul loro utilizzo solo attraverso *reverse engineering* o con documenti pubblici forniti dalle aziende.

Il **servizio di trasporto** da scegliere per un’applicazione dipende solo da che utilizzo ho di questa applicazione e di che compito devo svolgere (quindi che necessità ha). In base a questo concetti si possono avere delle necessità, riassumibili come:

* **perdita di dati**: bisogna capire se l’applicazione può tollerare qualche perdita di dati, oppure se ha la necessita di garantire un trasferimento affidabile di tutti i file passati attraverso la rete.

  In base a questo concetto di solito si propende per il servizio TCP che fornisce una garanzia sul ricevimento effettivo dei pacchetti.

* **throughtput**: alcune applicazioni hanno la necessità di una grande larghezza di banda, oppure tutta la larghezza disponibile. Altre invece necessitano di un ampiezza di banda minima per essere efficaci.

  Di solito un browser web non ha grandi limitazioni da questo punto di vista, cercando sempre di ottenere i dati in qualsiasi situazione di capacità della rete, mentre alcune applicazioni di streaming necessitano di un certo throughput per essere consistenti o utilizzabili.

* **sicurezza**: alcune applicazioni particolarmente sensibili hanno la richiesta della sicurezza, che ad oggi si può ottenere aggiungendo un elaborazione a livello software applicata ai dati ottenuti o offerti dalla/alla pila protocollare. Oppure si può utilizzare nuovi protocolli che permettono una connessione sicura aggiungendo di default alcuni strati alla pila.
* **temporizzazione**: alcune applicazioni hanno la necessita di essere “realistiche”, ovvero richiedono il più basso valore di ritardo possibile. Questa necessità però non è coperta da nessun tipo di servizio, nonostante si utilizzi la UDP siccome, non avendo una serie di controlli propri del servizio TCP, permette di essere più rapida nel collegamento.



Il livello di trasporto offre due servizi, in particolare, TCP e UDP. Il loro utilizzo dipende dal modello di applicazione che ho la necessità di creare: se io voglio navigare su internet necessito che la maggioranza dei dati siano corretti (anche a scapito della velocità). Altre applicazioni invece permettono di perdere dei dati (ad esempio la telefonia), riuscendo però a continuare la fornitura del servizio (infatti la comunicazione di voce non viene così rovinata da una perdita fino al 10%dei dati originali). 

![image-20211005182259302](image/image-20211005182259302.png)



### TCP

Il servizio TCP è utilizzato da gran parte delle connessioni a Internet, siccome ha come punto di forza la sicurezza della ricezione delle informazioni.

è caratterizzato da:

* **orientamento verso la connessione**: è richiesto un setup minimo tra client e server con il fine di avere un canale di comunicazione fisso e stabile, ovvero la verifica che il ricevente è attivo e disponibile alla ricezione per avere la garanzia di un traffico efficace.
* **trasporto affidabile**: controlla i processi di invio e ricezione per assicurare il corretto trasferimento dei pacchetti.
* capacità di **controllare il flusso**: il mittente (chi manda i dati) ha un meccanismo per cui riesce a conoscere lo stato del buffer del destinatario e quindi a non sovraccaricarlo.
* capacità di **controllare la congestione**: riesce a ottenere informazioni sullo stato di carico della rete, e quindi agire sui pacchetti in uscita per evitare di congestionare ulteriormente la comunicazione (eventualmente strozzando il traffico in uscita). Questo processo ha l’effetto collaterale di togliere il controllo sull’invio dei pacchetti all’utente.

Questo servizio però **non fornisce garanzie di temporizzazione** (anzi, spesso può subire dei rallentamenti per il controllo anticongestione in uscita) .

Di solito viene utilizzato nel browsing web o nella posta elettronica. Nonostante sembri sbagliato, anche lo streaming video sfrutta il TCP, siccome si vuole che questa grande quantità di dati non congestioni la rete. Per ovviare al questo problema si mettono dei convertitori che riescono a mandare il video nella rete a qualità differenti in base alla larghezza di banda disponibile.



### UDP:

Il servizio di UDP non fornisce nessun controllo fornito dal TCP, ma permette un **trasferimento dati inaffidabile** fra i processi d’invio e ricezione.

Questo servizio è nato per la necessità di avere un traffico a livello più basso possibile, infatti dovendo passare meno layer di controllo si ha la possibilità di avere più controllo sul pacchetto in uscita. Di fatto è come un protocollo più semplice, senza nessuna garanzia e nessun controllo.

Altre applicazioni sono ad esempio nella sensoristica, che utilizza questi servizi per evitare qualche gradino della pila TCP, che influirebbe sulle prestazioni o sulla batteria.

Viene anche utilizzato nella telefonia su internet, che ha la necessità di avere il ritardo più basso possibile.

Date le sue particolari caratteristiche del TCP (soprattutto senza il controllo di congestione) alcuni pacchetti UDP (troppo numerosi) possono essere droppati da nodi interni di controllo.

![image-20211005184222763](image/image-20211005184222763.png)

### TCP sicuro

Siccome le connessioni sicure sono sempre più importanti, ma la possibilità di avere un layer che fornisce sicurezza non è definito in nessun servizio (né TCP, né UDP), si è creato un nuovo servizio standard applicando degli aggiuntivi layer al servizio (preesistente) TCP.

Le applicazioni vanilla TCP/UDP non hanno crittografia. Quindi vuol dire che tutti i dati mandati e ricevuti sono mandati in chiaro (comprese le password).

Si utilizza quindi il **TLS (Transport Layer Security)** che si occupa di fornire connessioni TCP criptate per la comunicazione sicure di dati. Inoltre si occupa di *data integrity*, ovvero controlla che il pacchetto non sia stato in nessun modo modificato nel percorso dal mittente a ricevente, e fornisce il servizio di *end-point authetication*, ovvero la verifica che l’host con cui si sta parlando sia effettivamente chi dichiara di essere (questa misura rende gli attacchi di impersonificazione molto improbabili). 

Questo servizio è utilizzato da protocolli tipo l’`https` o l’`ssh` che  utilizzano il `TLS` proprio in modo da avere tutte le garanzie di questo servizio.

Questo layer è il responsabile di blocco in caso di rejecting site, ovvero quando viene annullata la connessione a un server che non dispone di certificati riconosciuti oppure inaffidabili (spesso ormai queste richieste vengono fatte direttamente dal browser). Questo tipo di blocco può capitare anche a siti che non hanno nessun problema di sicurezza, ma che semplicemente non hanno servito i certificati aggiornati o verificati. 

Il TLS può essere anche implementato a livello applicazione, utilizzando delle apposite librerie che si appoggiano al TCP, facendo in modo che in questo socket il traffico in uscita sia criptato. (Argomento molto complicato, che necessita di numerose conoscenza pregresse. Viene trattato nel capitolo 8 del libro).



## 2.2 Web e HTTP

Una pagina web è costituita da molti oggetti diversi, che possono essere un file HTML (Hypertext Markup Language, responsabile della descrizione della struttura della pagina, ovvero indica cosa contiene e una struttura indicativa), e una altra serie di file che possono essere foto, video e altri tipi di supporto:

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Questo spazio indica il titolo della pagina</title>
  </head>
  <body>
    All'interno di questa sezione ci sono i testi e gli elementi che compaiono nella pagina.
    Queste invece sono immagini e altri documenti che possono essere inseriti e verranno richiesti al server:
    <img src="image.png" alt="Image" align="middle" />
    <img src="image.png" alt="Image" width="100" />
    Audio
    <audio controls>
  	<source src="tutorial.ogg" type="audio/ogg" />
  	<source src="tutorial.mp3" type="audio/mpeg" />
  	Your browser does not support the audio element.
		</audio>

  </body>
</html>

```

Questo file HTML è la base della pagina, che contiene diversi oggetti referenziati. Ogni oggetto è referenziato da un **URL (Universal Resource Locator)**, ovvero il modello universale per indicare la localizzazione di un oggetto sulla rete.

L’URL di solito è formato da: (esempio con `www.someschool.edu/someDept/home.html`):

* `www.someschool.edu`: indica il nome dell’host
* `/someDept/home.html`: indica il nome del percorso

Il file HTML viene poi dato al browser che fornisce la rappresentazione della pagina con gli elementi descritti al suo interno (effettua il rendering).

(Sia chiaro che l’HTML è il formato con cui si scrivono come sono fatte le pagine web, non il protocollo con cui questi file vengono scambiati, che invece si chiama HTTP).

### HTTP

Questo protocollo si chiama **HyperText Transfer Protocol** e serve per scambiare file HTML tra due host. Di solito questa connessione è di tipo client-server, siccome uno scarica i file dall’altro. Dopo questa operazione questi file vengono visualizzati dal browser. 

Questo protocollo si appoggia al servizio TCP.

Di solito TCP fa uno scambio di pacchetto per assicurarsi la connessione e prosegue solo dopo a aprire la connessione logica con il server web. Tutte queste operazioni vengono fatte attraverso dei messaggi tra client e server. Questa socket utilizza la porta 80.

HTTP è dice protocollo **stateless**: ogni richiesta è a se e non viene mantenuto lo stato nel server che ha risposto a una richiesta. Questo vuo dire che per il server HTTP ogni richiesta è come un nuovo messaggio, senza avere una modifica delle pagine in base alla memoria della mia connessione. La scelta di creare questo protocollo in questo modo è stata fatta perché mantenere lo stato è più complesso e dispendioso di risorse per chi vuole fornire una risposta ottimale in un tempo accettabile. Inoltre nel caso di crash o errori da entrambe le parti si può avere la possibilità di avere due stati diversi e quindi non avere le connessioni sincronizzate (operazione non sempre facile soprattutto se si deve effettuare un unione dei due stati diversi).

Ci sono tecniche che permettono di fare delle pagine web a stato diverso in base all’utente utilizzatore, ma non attraverso il protocollo (ad esempio attraverso i cookie).

Di questo protocollo esistono diverse versioni, che forniscono gli host che utilizzano questi protocolli per comunicare con maggiore flessibilità o più velocemente.

Di base si hanno due tipi diversi di connessioni fornite dall’HTTP:

* **connessioni non persistenti**: forniscono la possibilità di scambio di un solo pacchetto e poi chiudono la connessione.

  Ottimale se si vuole ottenere solo un oggetto dalla zona richiesta.

* **connessioni persistenti**: forniscono una connessione con l’host che viene mantenuta per più pacchetti. Questa connessione verrà chiusa in condizioni particolari o in accordo con l’host connesso.

  Molto più comune nel web moderno, siccome uno stesso file HTML contiene molti più file al suo interno, che devono essere a loro volta scaricati, questo consente di scaricarli tutti utilizzando lo stesso “tunnel” e quindi non perdendo tempo a creare una nuova connessione con il server ogni pacchetto necessario.

Gli schemi di connessione HTTP con queste due modalità sono:

![image-20211005144701444](image/image-20211005144701444.png)

![image-20211005193823720](image/image-20211005193823720.png)

I passaggi *1a* e *1b* non avvengono a livello di protocollo, ma a livello di rete (sono create dal client TCP). Nel punto 3 il server analizza la risposta e se i dati sono disponibili e corretti risponde al messaggio, mentre qualsiasi altra risposta da queste presenti porteranno a una chiusura del collegamento. 

Alla fine di queste azioni la connessione termina (perché connessione non persistente). 

Alla necessità di un’altra richiesta al server si dovranno rifare tutti questi processi un’altra volta, siccome la rete è stateless e quindi non si ricorda la risposta precedentemente fornita.

![image-20211005145020605](image/image-20211005145020605.png)

A caratterizzare il tipo di protocollo utilizzato è anche **RTT (Round Trip Time)**, ovvero il tempo necessario da quando viene mandata la richiesta a quando si ottiene la risposta.

Il tempo totale di risposta poi si calcola, in connessione non persistente, come $2RTT + \text{tempo di trasmissione}$.

Infatti le connessioni non persistenti hanno, oltre alla lunghezza sempre finita di connessione per ottener un pacchetto (ma che viene sempre ripetuta completamente) la caratteristica di aggiungere overhead al sistema operativo per ogni connessione TCP. Questo però permette di avere connessioni multiple e parallele con il server per scaricare oggetti (o lo stesso oggetto più velocemente).

Le *connessioni persistenti* invece hanno un tempo di risposta per pacchetto di $RTT + tempo di trasmissione$, siccome il server lascia la connessione aperta dopo la ricezione del primo segnale, lasciando i successivi pacchetti viaggiare nello stesso “tunnel”. Il client quindi è libero di inviare richieste ogni volta che incontra un oggetto referenziato senza la necessità di aspettare la creazione di un nuovo layer di connessione.



Logicamente il lato positivo del secondo tipo di connessione si ottiene solo se si ha la necessità di fare più connessioni consecutive, perché in caso contrario si ha lo stesso tempo di risposta per ottenere i pacchetti.

Il lato negativo di connessioni continuative sono che le risorse sono bloccate dal lato server e in casi particolari il server può non riuscire più a soddisfare le richieste di tutti gli host in connessione.

Esistono anche dei pacchetti chiamati *keep-alive* con lo scopo di mantenere una rete attiva. Di solito viene utilizzato questo metodo per mantenere una connessione operativa anche se è presente scarso traffico su quella linea. Per questo motivo è molto spesso utilizzata da collegamenti tra strumenti non con richieste di utenti. Questo tipo di pacchetto è necessario siccome ogni rete ha un tempo oltre al quale droppa la connessione e se si vuole continuare a comunicare la si deve ricreare. Nell’utilizzo normale il servizio TCP si occupa di questa gestione.

#### Messaggi HTTP

Il protocollo HTTP è diviso in due tipi di messaggi: **richiesta** e **risposta**, inoltre sono tutti in **formato testo ASCII**, ovvero leggibili dagli utenti.

##### Richiesta HTTP

```http
GET /somedir/page.html HTTP/1.1
Host: www.someschool.edu
User-agent: Mozilla/4.0
Connection: close
Accept-language: fr

<body>
```

Dove nella prima riga sono presenti:

* la definizione del **METODO** (in questo caso `GET`)
* indicazione delle location **URL**
* **versione protocollo HTTP** (che può essere `1`, `1.1` o `2`) 

Seguono le **righe di intestazione**:

* `Host`: conferma l’host alla quale siamo collegati.
* `User-agent`: segnala l’engine che gestisce il browser, in modo da fornire il miglior formato HTML per il supporto richiesto.
* `Connection`: indica il tipo di connessione desiderata. Questo valore può essere `keep-alive` se si vuole mantenere aperta oppure `close` se invece si vuole chiudere la connessione. (`close` è default nelle richieste HTTP/1.0).
* `Accept-language`: indica la lingua della risposta, in modo da fornire dei dati più soddisfacenti al client (se disponibili). 
* infine per indicare la fine, si inserisce un `carriage return` e `line feed` extra che indicano che le informazioni della richiesta sono state inviate.
* `<body>`: indica la possibilità di dati associati alla richiesta fatta. Il fatto che questa parte ci sia o meno dipende dalla prima riga, che indica di conseguenza se aspettarsi o meno la ricezione di questo eventuale pacchetto.



##### Metodi HTTP

Ci sono diversi metodi di richiesta verso il server, che possono indicare il fatto che si vuole aggiungere della informazioni al server o che si vogliono fare altre operazioni. Questi metodi sono (dal protocollo `HTTP/1.0`):

* `GET`: indica che si vuole riceve un valore contenuto nel server
* `POST`: indica che si vuole aggiungere un valore al server (di solito utilizzato per fare l’upload di file e mandare form)
* `HEAD`:  si richiedono al server *solo* gli header del file, senza l’effettivo corpo.

dal protocollo `HTTP/1.1` in poi hanno aggiunto:

* `PUT` : si vuole fare una richiesta al server (ma a differenza di `POST`, più richieste uguali verranno scartate e se ne manterrà una sola)
* `DELETE`: si utilizza per cancellare dati dal server



*Per mandare un form perè ci sono altri metodi:*

* o utilizzo il metodo *`POST`* attraverso HTTP

* o utilizzo metodo `GET` attraverso `URL` 

  si utilizzava più un tempo, ma anche oggi si può incontrare questo metodo in qualche caso.

  ```http
  www.somesite.com/animalsearch?monkeys&banana
  ```

  Questo significa che se faccio un `GET` di questo URL allora sto facendo una ricerca e due parametri ricercati sono `mokeys` e `banana`. Di solito si utilizza spesso questo metodo se la pagina è “attiva”, ovvero che ci sono alcuni valori della pagina che possono cambiare in base alle richieste.




##### Messaggio di risposta HTTP

```http
HTTP/1.1 200 OK 
Connection close
Date: Thu, 06 Aug 1998 12:00:15 GMT 
Server: Apache/1.3.0 (Unix) 
Last-Modified: Mon, 22 Jun 1998
Content-Length: 6821
Content-Type: text/html

<data>
```

La prima riga indica:

* il **protocollo utilizzato**
* il **codice di stato** della richiesta

Poi seguono la serie di informazioni degli header, terminati da un `carriage return` e un `line feed` che indicano la fine degli header. Poi seguono tutti i dati di risposta.

I *codici di stato* della risposta HTTP sono diversi, e hanno tutti un significato e un uso ben preciso:

* `200 OK`: richiesta ha avuto successo, l’oggetto richiesto viene inviato
* `301 Moved Permanently`: l’oggetto richiesto è stato trasferito; la nuova posizione è specificata nell’intestazione `Location:` della risposta
* `400 Bad Request`: Il messaggio di richiesta non è stato compreso dal server 
* `404 Not Found`: Il documento richiesto non si trova su questo server
* `505 HTTP Version Not Supported`: Il server non ha la versione di protocollo HTTP

Questo collegamento si può provare a lato client con `telnet`, attraverso questo comando si può aprire una connessione con un server e una porta `telnet www.unitn.it 80`. Inserendo e mandando degli header HTTP si otterranno le risposte, come si può vedere da questo esempio:

```bash
telnet www.unitn.it 80
GET / HTTP/1.1
Host: www.unitn.it


HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Mon, 11 Oct 2021 13:42:13 GMT
Content-Type: text/html
Content-Length: 162
Connection: keep-alive
Location: https://www.unitn.it/

<html>
<head><title>301 Moved Permanently</title></head>
<body>
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx</center>
</body>
</html>
```

In questo caso ritorna un codice `301 Moved Permanently` siccome è un sito `https` e quindi dovrebbe essere sulla porta `443` definitivamente.



##### Cookies

Nonostante HTTP non permetta il mantenimento dello stato della connessione, si può ottenere questa funzione utilizzando i **cookies**. Questo valore viene inserito all’interno dell’header di qualche messaggio e contiene un valore univoco, conosciuto dal client e dal server. Questo valore incrociato con i risultati di un database permette la personalizzazione di una pagina web, fornendo gli stessi dati allo stesso valore di un determinato cookies.

I cookies sono utilizzati da molti dei più importanti siti web.

Un esempio di utilizzo:

![image-20211011154607082](image/image-20211011154607082.png)

Queste informazioni vengono chiaramente salvate sia da parte client e server. I dati salvati dalla parte client  sono facilmente accessibili dall’user e quindi facilmente eliminabili. Le informazioni salvate sul server invece sono tutte completamente sotto il controllo dell’amministratore del server e quindi il loro utilizzo può cambiare in base a che utilizzo ne fa l’azienda che lo controlla (ad esempio si possono sfruttare con il fine di profilare l’utente finale del servizio).

Questa funzione è molto utile, soprattutto nel caso di molte azioni sullo stesso sito che necessitano di una connessione autenticata, infatti permette di eseguire una serie di richieste al server senza la necessità di ripetere tutte le volte le informazioni di login.

Di solito i cookies hanno un tempo di vita, ma si può decidere di cancellarli manualmente.

I cookies funzionano attraverso 4 componenti principali:

1. una riga di intestazione nel messaggio di *risposta* HTTP
2. una riga di intestazione nel messaggio di *richiesta* HTTP
3. un file cookie mantenuto sul sistema terminale dell’utente e gestito dal browser dell’utente
4. un database sul sito

Al loro interno possono contenere una serie di informazioni, come:

* autorizzazione
* carta per acquisti 
* raccomandazioni 
* stato della sessione dell’utente (e-mail) 

Lo stato di queste informazioni, trasportato dalle richieste HTTP, servono per mantenere costante dei valori condivisi tra client e server che sono necessari alla comunicazione a stato costante.

Per la creazione di cookies sono necessarie delle azioni ben precise:

* il server risponde alla richiesta includendo nell’header HTTP il valore univoco di identificazione:

  ```http
  Set-cookie: <value>
  ```

* Il browser vedendo questo valore nell’header e aggiunge il valore di questo cookie nel suo gestore.

* ogni volta che il browser invia una richiesta al server, allora richiede al gestore dei cookies il valore cercato e lo inserisce nella richiesta HTTP attraverso l’header, con:

  ```http
  Cookie: <value>
  ```



##### Cache Web o server proxy

Un server proxy o un server di cache serve per mantenere una copia del sito che si vuole visitare più vicino all’utente finale, in modo da velocizzare la richiesta e/o oltrepassare dei colli di bottiglia che ci possono essere sulla rete esterna o direttamente sul collegamento principale.

Questo server funziona intercettando tutte le richieste HTTP (perché questo avvenga deve essere configurato sull’host o sul gestore della rete, in modo che ogni computer che si collega sappia di dover far passare tutto il traffico attraverso questo server). Una volta ricevute queste richieste cerca di esaudirle in locale, attraverso dati precedentemente scaricati (per questo motivo si deve avere una grande disponibilità di memoria).

Questa funzione ha una serie di lati positivi, ma lascia anche una serie di questioni irrisolte, ad esempio la distcrepanza che può crearsi tra sito reale e sito “copiato” in locale.

Infatti questo sistema permette da un lato di avere una connessione (risultanti) più performante, in modo da 1) non sovraccaricare la rete scaricando la stessa pagina molte volte, 2) riduce il tempo di risposta, 3) minimizzando l’utilizzo del collegamento alla rete, scaricando solo quello necessario alla risposta delle richieste.

Il funzionamento generale di questo server è:

1. il browser stabilisce una connessione TCP con il proxy, fornendo le indicazioni del sito che si vuole visitare
2. Il proxy controlla se è presente tra le copie memorizzate localmente, se lo è allora ritorna l’oggetto con un pacchetto HTTP
3. Se il server non ha l’oggetto, allora il proxy apre una connessione con il server finale, ottiene il valore cercato, ne salva una copia in locale e poi ritorna il pacchetto via HTTP al richiedente.

Di solito questi server sono mantenuti dagli ISP o da grandi centri di distribuzione, fornendo un modo per “salvare” delle connessioni particolarmente poco connesse o con una banda ridotta. In questo permettono di aumentare la banda disponibile e anche di ridurre i costi di gestione (siccome i pacchetti sono più vicini, il server e le comunicazioni saranno più veloci).

Questi utilizzi di server di cache sono molto utilizzati anche nelle infrastrutture di CDN (Content Distribution Networks), che utilizzano queste tecniche per velocizzare il servizio e la latenza media.

Un esempi di utilizzo server di cache in modo utile:

![image-20211011160844590](image/image-20211011160844590.png)

![image-20211011191042019](image/image-20211011191042019.png)

![image-20211011191057324](image/image-20211011191057324.png)

Nonostante questo operazioni portino moltissimi lati positivi per l’utente finale, il server si deve occupare di mantenere la copia aggiornata rispetto al valore reale. Per favorire questa operazione nell’header del HTTP c’è un valore che serve a verificare se il valore di cache è aggiornato. Questo meccanismo di controllo è chiamato **conditional GET** (lo scopo logicamente è quello di fare meno richieste possibili all’esterno della rete).

Questa richiesta HTTP chiamata appunto conditional GET richiede al server finale solo queste informazioni:

```http
GET / HTTP/1.1
Host: www.get-this-host-down.com
If-modified-since: Wed, 9 Sep 2015 09:23:24 
```

Ovvero la data che il proxy ha salvato la pagina in locale. Se il server nota un cambiamento della pagina tra questa data e la versione attuale, allora risponde con un oggetto, in caso contrario risponde con un codice `304 Not Modified` e quindi il proxy mantiene la copia originale.

Altri accorgimenti da parte del server possono essere introdotti con questi altri valori inseriti nella risposta:

```http
Cache-Control: max-age-<seconds>
Cache-Control: no-cache
```

che informano l’host se consentire di effettuare la cache, e in caso per quanto è consigliato mantenerla/dopo quanto è consigliato aggiornarla.

Il proxy può fare questo tipo di controllo tutte le volte che si desidera in base a quanto sia importante avere dei dati aggiornati piuttosto che veloci. 

Con un proxy di cache comunque ci può essere un piccolo intervallo di tempo in cui i dati sono “antichi”, ovvero non sono coerenti con il buffer temporale di adesso (si aggiornano semplicemente aggiornando la pagina, perché una volta che si richiede nuovamente questo sito, il server avrà fatto il controllo al server reale e, in caso, scaricato la copia originale e aggiornata).

Esempio di connessione *conditional GET*:

![image-20211011162620804](image/image-20211011162620804.png)

### Aggiornamenti e miglioramenti di HTTP

#### HTTP/1.1

Rispetto al protocollo HTTP/1 introduce la possibilità di avere una connessione aperta per tutta la durata di multipli trasferimenti, per velocizzare il download di contenuti di un unica pagina. (Get multipli in pipeline, ovvero su una singola connessione TCP).

Si inseriscono dei metodi e dei codici nuovi, come PUT e DELETE.



#### HTTP/2

Rispetto al protocollo HTTP/1.1, in questa versione, si cerca di diminuire il ritardo in caso di richieste di oggetti HTTP multipli. Tutti i comandi, codici e metodi del HTTP/1.1 rimangono invariati.

Questo protocollo cerca di risolvere un problema della rete per l’utilizzo di dati sempre più pesanti e grandi. Di solito i server funzionano con il metodo **FCFS (First Come First Served)**, ovvero viene servito il primo oggetto in cima alla coda di richieste. Questo può portare disfunzioni per gli altri utenti della rete, se uno di questi richiede un oggetto molto grande e quindi che necessita di molto tempo per la comunicazione, ancora superiore se connessione richiesta come TCP, che dovrebbe recuperare eventuali errori di trasmissione.

Questo problema si chiama **HOL blocking**, ovvero **head-of-line blocking**. 

Per risolverlo, si utilizza la divisione di oggetti molto grandi in frame, che vengono mandati con frequenza diversa, lasciando quindi lo spazio agli altri pacchetti di circolare. L’effetto finale è che il pacchetto più grande sembrerà leggermente più lento, ma la rete nella totalità sarà più reattiva.

Di fatto questo protocollo modifica l’ordine di importanza dei pacchetti e quindi l’ordine con cui vengono inviati.

![image-20211011163129591](image/image-20211011163129591.png)



#### HTTP/3

Il protocollo HTTP/2 viene presto soppiantato dal protocollo HTTP /3 che inserisce alcuni miglioramenti rispetto al predecessore: aggiunge la sicurezza, il controllo di congestione degli errori sul singolo oggetto UDP.

In realtà molte modifiche di questo protocollo non avvengono a livello applicazione, ma a livello di trasporto (e quindi verranno trattate più avanti nel corso).



### Evoluzione dal HTTP: protocollo QUIC

Data la difficoltà nell’aggiornare il protocollo HTTP, si è studiato un modo migliore per scambiare dati, avendo una serie di caratteristiche necessarie nel mondo di oggi.

Google lo ha inizialmente disegnato e proposto nel 2012, implementato subito come protocollo di connessione di Chrome per connettersi ai server Google (subito implementato anche da Firefox, Edge chrome version e Safari). 

Questo protocollo si basa sull’UDP, ma viene anche spesso nominato TCP/2 per le numerose somiglianze che alla fine si ottengono, siccome sono stati poi implementati una serie di controlli tipici del TCP.

QUIC implementa la possibilità di ridurre la latenza, attraverso la stima della banda disponibile da entrambe le direzioni, implementando algoritmi di nuova generazione per il controllo delle congestioni (sviluppato dal punto di vista users, che permette uno sviluppo più rapido). 

Questo protocollo supporta le connessioni sicure e multiplexate, in modo da fornire banda e sicurezza per una connessione sicura il più veloce possibile.



## 2.3 FTP

Il protocollo per trasferire i file, in maniera affidabile (in modo che i file siano uguali da server a client). Questo protocollo si appoggia a TCP.

Le informazioni di questo protocollo sono contenute nel `RFC: 959`.

Questo protocollo consente la connessione per scaricare dei file attraverso una connessione doppia: a porta `21` un gestore si interfaccia con il server, mentre i file veri e propri vengono spostati sfruttando la porta `22` (che viene aperta solo in caso di spostamento di file). 

Questo protocollo si utilizza tra client e server, ma spesso si utilizza un’interfaccia di più alto livello che permette di svolgere azioni senza interagire direttamente con questi due servizi.

Il fatto che le connessioni per la gestione e il passaggio di dati vengono divise tra due porte si oppone direttamente alla tecnica di HTTP di mandare dati (in unico pacchetto che contiene tutti i valori necessari).

Il server FTP mantiene lo stato della connessione: riesce a mantenere la directory corrente, le autenticazioni e altre informazioni utili.

I comandi più comuni sono inviati come test ASCII sulla connessione di controllo, e possono essere:

```http
USER username ; manda informazioni dell'user
PASS password ; manda informazioni della password
LIST 		  ; elenca gli elementi di una directory
RETR filename ; recupera il file nella direcoty corrente
STOR filename ; memorizza file nell'host remoto
```

mentre i codici di ritorno più comuni sono:

* `331 Username Ok password required`
* `125 data connection alredy open; transfer starting`
* `425 Can’t open data connection`
* `452 Error writing file`



## 2.4 Posta Elettronica

Per il funzionamento della posta elettronica sono necessari 3 componenti principali: 

* **agente utente**: 
  * detto anche “mail reader”
  * ha la possibilità di comporre, editare e leggere i messaggi di posta elettronica (su un agente dedicato, come Outlook, Thunderbird, oppure il browser attraverso un interfaccia)
  * i messaggi in arrivo e in uscita sono memorizzati su un server
* **server di posta**: contiene la 3 componenti:
  * *casella di posta* (mailbox): contiene i messaggi in arrivo per l’utente
  * *coda di messaggi* da trasmettere
* **protocollo SMTP (Simple Mail Transfer Protocol)**: protocollo di base per consegnare le mail tra server di posta elettronica.
  * Sono lo stesso costituiti da server e client, (uno che manda e uno che riceve). Nonostante questa caratteristica i server SMTP possono essere bi-direzionali.
  * Utilizza il servizio TCP.
  * Il protocollo è definito dal `RFC 5321`

Le mail sono dei mezzi di comunicazione asincrona (le persone mandano e ricevono messaggi quando più gli conviene, senza doversi organizzare con altri server e users). Le mail sono veloci, facili da mandare e senza particolari costi rispetto alle poste. Inoltre le nuove versioni permettono anche l’invio di dati molto complicati, che possono includere allegati, testo in formato HTML e foto inserite all’interno. 

Nonostante sia un protocollo molto utilizzabile e a cui è stato aggiunto molto supporto nel corso degli anni, ci sono comunque una serie di restrizioni che sono portate dalla sua creazione, principale su tutto: trasmisione fissa a 7-bit ASCII char. Questa limitazione è particolarmente sentita oggi, siccome tutte le foto e le immagini, oltre che gran parte dei contenuti multimediali sono contenuti in un buffer di 8-bit.

Questo impone che il server codifichi gli allegati al momento di mandare la mail, e li decodifichi al momento di ricezione del messaggio. Questo problema si nota dalla dimensione dei messaggi che prima della codifica sono più piccoli.

Le connessioni SMTP di solito sono mandate direttamente, senza passare per nessun server secondario, costituendo quindi di fatto una connessione peer-to-peer per il passaggio di dati.

Le caratteristiche di connessione inoltre dispongono che, in caso di server irraggiungibile, il messaggio verrà mandato in un altro momento, riprovando finché non si ottiene un esito positivo.

La porta predefinita per queste connessioni è la `25`.

Un esempio di una connessione SMTP può essere:

```http
S: 220 hamburger.edu
C: HELO crepes.fr
S: 250 Hello crepes.fr, pleased to meet you
C: MAIL FROM: <alice@crepes.fr>
S: 250 alice@crepes.fr ... Sender ok
C: RCPT TO: <bob@hamburger.edu>
S: 250 bob@hamburger.edu ... Recipient ok
C: DATA
S: 354 Enter mail, end with ”.” on a line by itself
C: Do you like ketchup?
C: How about pickles?
C: .
S: 250 Message accepted for delivery
C: QUIT
S: 221 hamburger.edu closing connection
```

Anche questo servizio può essere testato con `telnet serverName 25`.

I comandi più utilizzati di questo protocollo sono `HELO`, `MAIL`, `FROM`, `RCPT TO` , `DATA`, `CRLF`, `QUIT`. 

Questo protocollo è abbastanza complicato da implementare a mano, siccome tutti i comandi devono essere mandati nel momento giusto con la giusta sintassi.



In questo protocollo ci sono alcune somiglianze e altre differenze con il protocollo HTTP:

* HTTP serve a passare oggetti dal server al client, oggetti chiamati pacchetti.  Di solito la connessione è tra un server e client(browser).
* SMTP si connette direttamente da un server a un altro e fornisce una comunicazione diretta.
* HTTP e SMTP utilizzano connessioni persistenti.
* con HTTP è molto più utile utilizzare la modalità di PULL (ovvero richiedere dati dal server), mentre con SMTP è molto più comune fare operazioni di PUSH.
* SMTP impone la condizione di avere caratteri ASCII a 7-bit, mentre HTTP non impone limiti
* SMTP gestisce diversamente gli hypertext, a differenza di HTTP che li inserisce in un pacchetto e li manda al richiedente.



### Messaggi SMTP

Il protocollo SMTP crea dei messaggi che seguono un formato ben definito. Queste caratteristiche sono tutte descritte nel `RFC: 822`, e di base contiene due “parti” di messaggio:

* le righe di intestazione: `TO/a`, `FROM/da`, `Subject/oggetto` e un altra serie di parametri che servono a indicare la presenza (e in questo caso il tipo) degli allegati, e il tipo di encoding utilizzato per mandare i dati (di solito base64, che è lo standard per fare conversione da 8 a 7 bit).
* il corpo: contiene il messaggio con le caratteristiche identificate prima

```html
From: alice@creps.fr
To: bob@hamburger.edu
Subject: Just-Eat order
MIME-Version: 1.0
COntent-Transfer-Encoding: base64 // Codifica per i 7 bit
Content-Type: image/jpeg 		  // indica i sottotipi di dati, oppure i dati multimediali presenti come allegati

base64 encoded data ....

// se più di un oggetto ci sono dei marcatori che separano gli oggetti mandati fra di loro
```



### Protocolli di accesso alla posta

Mentre SMTP si occupa di tutta la sezione di invio, trasporto e consegna. Una volta che il messaggio viene recapitato però ci sono più modi per gestire l’effettiva ricezione del destinatario. Questi 3 modelli sono:

* **POP (Post Office Protocol)**: segue `RFC 1939`
* **IMAP (Internet Mail Access Protocol)**: segue `RFC 1730`
* **HTTP**: per accedere alla posta via browser (ad esempio Gmail).

Questi modelli si fondano sul fatto che, una volta mandato il messaggio, il ricevente può decidere quando riceverlo (lasciando quindi la possibilità al mittente di inviare il messaggio quando è più comodo, senza occuparsi del ricevente).

Al momento il più utilizzato è l’accesso HTTP, siccome molto servizi di posta forniscono un’interfaccia per accedere, modificare e organizzare le mail dal browser.



#### POP:

Questo protocollo è generalmente utilizzato con dei gestori di posta. Si occupa di gestire l’autorizzazione di agente e server per il download della posta. Questo protocollo non mantiene lo stato sul server (ma lo può mantenere sul client) e per questo motivo utilizza il metodo *read-and-delete*, ovvero le mail scaricate vengono rimosse dal server.

Nonostante queste impostazioni di default, il protocollo può anche essere impostato per mantenere tutte le mail sul server, che siano state lette o meno, ma questo impone che il client abbia un metodo per selezionare solo quelle da leggere per rendere comprensibile all’utente quali mail siano ancora da aprire. Inoltre la massa di dati che si genera in questo modo, avendo una casella di posta molto grande, rende lungo e difficile il download di tutti i dati tutte le volte che si richiedono.

Questo protocollo ha inoltre il problema che se si utilizza il metodo *read-and-delete* sul server, per avere un database consistente di mail, nel quale compaiano tutte le mail ricevute, allora è necessario avere un unico punto di download delle mail che è unico per questo scopo (concetto anacronistico al mondo d’oggi).

Questo protocollo è stato il più utilizzato nei primi di anni di utilizzo della posta elettronica.



#### Funzionamento del Protocollo POP3:

Il POP3 è il protocollo più nuovo del modello POP. Sarà l’unico protocollo per la gestione del download della posta visto in questo corso.

Tutta la comunicazione utilizza solo caratteri alfanumerici. Si da per scontato sia la configurazione di base, quindi i download e la cancellazione della mail.

Questo protocollo segue `RFC: 1939`.

```html
telnet mailServer 110
+OK POP3 server ready
user bob
+OK
pass hungry
+OK user successfully logged on
```

Nel caso di errori, il server risponderà con `- ERR`.

Se l’autenticazione va bene invece, si procede al download-and-delete, effettuato con questi tre comandi `list`, `retr` e `dele`.  Un esempio di questa connessione può essere:

```http
C: list
S: 1 498
S: 2 912
S: .
C: retr 1
S: (blah blah ...
S: .................
S: ..........blah)
S: .
C: dele 1
C: retr 2
S: (blah blah ...
S: .................
S: ..........blah)
S: .
C: dele 2
C: quit
S: +OK POP3 server signing off
```

In realtà questo protocollo ha uno “stato parziale”, infatti tutte le modifiche di eliminazione e modificazione vengono effettivamente svolte solo una volta chiusa la connessione. Per questo motivo i comandi effettivamente portano un valore di stato a indicare se sono state scaricate o meno.

Grazie a questa mancanza di stato è molto più facile l’implementazione e il suo funzionamento in generale, nonostante porti a una mancanza sostanziale del supporto multipiattaforma per un servizio che, ad oggi, lo richiede.

#### IMAP

Il protocollo IMAP ha lo stesso scopo del protocollo POP, ovvero permettere il download delle mail dal server di ricezione, ma si comporta in maniera completamente diversa.

Questo protocollo ha la maggiore differenza nel fatto che mantiene lo stato, e quindi si ricorda quali messaggi sono stati inviati, letti e non letti. Si può anche ricordare di regole di smistamento, di cartelle per organizzare le mail archiviate e in arrivo.

Nonostante sia stato sviluppato prima del POP questo protocollo è più moderno per le caratteristiche che offre, permettendo la sincronizzazione su più dispositivi di tutte le mail ricevute. 

Inoltre questo protocollo ha il successivo lato positivo che non si impone come gestore delle mail, ma fa in modo di rilasciare tutto il compito di gestione al server di ricezione.



## 2.5 DNS (Domain Name System)

Un sistema di computer, una volta arrivato alla mano della comunità ha dovuto cercare un modo migliore per nominare gli host, siccome i singoli indirizzi IP potevano essere molto difficili da comprendere. Per questo motivo hanno introdotto **hostname** (ad esempio: `www.facebook.com`, `www.google.it`). 

Nonostante questa convenzione sia molto più comoda per la lettura e la comprensione umana degli indirizzi, non lo era altrettanto per i router, che invece lavorano meglio con i singoli indirizzi IP (un indirizzo IP è un numero costituito in questo modo: `112.134.243.199`, dove ogni valore separato dal punto è compreso tra 0 e 255).

Siccome queste due necessità devono esistere nello stesso momento, allora serviva creare un servizio che procedesse a questa “traduzione” (da IP a hostname, oppure da hostname a IP). Questo servizio è stato chiamato **DNS (Domain Name System)** e è completamente implementato sui nodi esterni della rete.

Questo servizio è un database distribuito implementato su una gerarchia di server DNS che lavora insieme a un protocollo che permette di effettuare una query distribuita su questi database.

Di solito il DNS viene utilizzato molto per applicazioni che utilizzano HTTP e SMTP. Le azioni che portano a ottenere una risposta DNS sono:

* l’applicazione sul computer richiede un servizio e passa la richiesta al DNS client, perché la traduca.
* il DNS client manda una richiesta a un server DNS.
* il DNS client riceve una risposta con l’IP dell’hostname ricercato.
* una volta ricevuto questo indirizzo, allora si inizia a creare la connessione TCP

E’ interessante pensare che come si è iniziato a fare negli anni 70, la complessità di un servizio del genere non viene relegata all’interno della rete, ma si appoggia solo sui nodi di accesso e quelli perimetrali. In particolare questo modo di fare è stato molto utile nel caso dei server DNS, siccome è un servizio che può spesso cadere (per le moltissime richieste che può ricevere), e in questo caso nessun host che necessita di una “traduzione” (operazione di conversione IP-hostname, oppure hostname-IP) è più possibile e quindi diventa difficile accedere a qualsiasi punto della rete di cui non si conosce esplicitamente l’IP.

Per limitare questi avvenimenti si sono sviluppati degli accorgimenti negli anni che mirano a rendere questo servizio il più stabile e efficace possibile, aggiungendo dei livelli di cache sempre più vicino all’utente finale con un indice degli indirizzi più cercati in locale.



### Servizi DNS:

Il DNS fornisce diversi servizi:

* tradurre hostname a IP, e inverso.

* **host aliasing**: avendo un sistema di traduzione, si può anche pensare che un hostname molto utile si può anche salvare con una variante diversa di questo nome e fatta puntare allo stesso server. Si otterrà quindi che quel server ha un hostname multiplo, ma che riporta alla stessa macchina alla fine.

  Ad esempio, questo tipo di servizio può servire per nomi di server molto lunghi: `enterprise-server.com` serve la stessa macchina che internamente è chiamata `relay1.west-coast.enterprise.com`.

* Un importantissima applicazione di questo servizio è il **load balancing**, ovvero quando diversi server che servono la stessa applicazione si dividono il carico di richieste in entrata per fornire delle risposte più veloci e evitare un eventuale crash del sistema per un carico di dati esageratamente alto. Inoltre questa modalità di organizzazione server è molto utile per mantenere sempre attiva un applicazione, siccome si avrà sempre a disposizione un’altra macchina per fornire una risposta.

  Questa applicazione dei servizi DNS è molto utilizzata nei server cloud, che permettono di aver lo stesso servizio replicato più volte, in base al traffico di ingresso. Per collegarsi a questi servizi però si utilizzerà lo stesso *hostname*, anche se questo valore riporta a indirizzi diversi in base a quale macchina il *load balancer* (programma per direzionare il traffico, dividendolo tra le macchine) decide che è più scarica di richieste.

  Altre aziende, come Akamai, hanno trovato modi per utilizzare questa azione del DNS in modo molto più sofisticato, per permettere la scalabilità dei sistemi e il funzionamento di altri servizi che offrono.

* **mail server aliasing**: allo stesso modo del name-server aliasing si ottiene un servizio che possa restituire lo stesso indirizzo di posta, anche se questo è molto scomodo e lo si è sostituito con uno più facile.



### Funzionamento: richieste e risposte del DNS

Questo servizio di traduzione è utilizzato dal 1970, per questo motivo ha diversi RFC che lo riguardano. Di base ci si può riferire al `1034` e il `1035` in particolare.

Il sistema è molto complesso, quindi si noteranno solo i punti chiavi delle azioni che svolgono questi server riferite solo all’azione di traduzione hostname-IP.

Intanto bisogna notare che il DNS è un **application-layer protocol** (quindi come HTTP, FTP, ...). Tutte le richieste sono mandate attraverso UDP sulla porta 53.

Il modo più semplice per implementare questo sistema è quello di avere un server centralizzato che possa rispondere a tutte le richieste, ma questa architettura (anche se alletta il fatto che sia così semplice) ha molti lati negativi:

* **single point-of-failure**: se questo serve va in crash, allora tutti gli hostname esistenti sono irraggiungibili da tutti.
* **volume di traffico**: questo server da solo gestirebbe un volume di traffico che serve per milioni di utenti.
* **database centralizzato localizzato**: un database centralizzato per definizione avrà un area di vicinanza e una di lontananza. Questa organizzazione dei server quindi fornirebbe un servizio che ha prestazione molto varianti in base alla posizione del client.
* **update e maintenance**: questo server sarebbe grandissimo, richiederebbe una complicata e lunga operazione di manutenzione (con la possibilità di lasciare tutti gli host senza accesso alle sue informazioni). Inoltre sarebbe molto spesso in update, siccome dovrebbe ogni volta aggiungere le informazioni degli host DNS tutte le volte che vengono aggiornate dai singoli user.

Di fatto questi lati negativi si possono riassumere con il concetto che un server centralizzato non può essere scalabile.

La soluzione a questi problemi arriva con la costruzione di un sistema decentralizzato e non gerarchico. Ci sono 3 livelli di server DNS:

* **root DNS server**: sono circa 400 server disposti in tutto il mondo in modo da fornire più server dove c’è più richiesta (dove ci sono più utenti), gestiti da 13 organizzazioni.

* **TLD (top level domain) DNS server**: sono ad esempio i server che mantengono le informazioni per gli hostname che terminano per `.com`, `.edu`, etc., ma anche `.it`,`.uk`, etc.. Di solito questi server sono gestiti da privati che hanno interessi a mantenere il loro server, oppure da organizzazioni nazionali o internazionali.

* **authoritative DNS server**: sono i server di una qualsiasi organizzazione che necessita di avere host accessibili dalla rete necessita. Può decidere di gestire questi server internamente (se ad esempio è un azienda molto grande e necessita di molti hosts sulla rete), oppure di affidarsi a un azienda che offre questo servizio. 

  Spesso le grandi aziende mantengono e supportano il server primario e secondario (server di backup) che mantiene le informazioni per i loro hosts.

Organizzati a albero, con come radice i *root DNS server*. Un *hostname* è scritto in questo modo: `www.unitn.it`, e si possono trovare al suo interno questi server diversi:

* top-level domain: `it`
* authoritative server: `unitn`, che in realtà contiene anche le informazioni per `www.unitn`.

Durante una normale DNS query, si server **DNS radice** è il primo che si va a contattare (anche se non viene richiesto direttamente dall’utente). Questi server non hanno mai tutte le informazioni necessarie a fare già l’associazione hostname-IP, ma sono l’entry-point per inziare la query. Siccome ci sono 13 associazioni che gestiscono questi server, si può pensare che ci siano 13 punti di ingresso logici per iniziare la ricerca.

Siccome le informazioni non sono quasi mai presenti sui *root* server, allora si passa la richiesta ai *top-domain* server. Questi server possono restituire le indicazioni per trovare il server di competenza (*authoritative*) che sappia rispondere alla query. Questi server sono spesso gestiti e mantenuti dagli ISP regionali.

Tutta queste operazioni servono solo per recuperare l’indirizzo fisico del server, quindi possono essere bypassate se si ricorda direttamente l’indirizzo IP. La pratica di ricordare gli indirizzi risulta molto utile sia in caso di caduta del server, sia per evitare di dover accedere tutte le volta al server root e svolgere l’intera query tutte le volte. La funzione di ricordare gli indirizzi viene mantenuta di solito da un server di cache (con un funzionamento uguale al cache proxy server del protocollo HTTP). Di solito tutte le intranet hanno un DNS proxy locale, molto spesso anche il router casalingo mantiene un semplice proxy DNS ormai.

Questa usanza inoltre permette di ridurre il tempo necessario alle richieste DNS, siccome si avvicinano le informazioni all’user finale, permettendo una navigazione più veloce.

Un attacco DDoS contro un server DNS sarebbe molto effettivo per tutti gli utenti che utilizzano quel server per tradurre gli hostname, motivo per cui è un attacco che spesso si nota. Per ridurre il livello di impatto di questo tipo di attacchi si utilizza spesso un load-balancer prima dell’accesso del server, che redireziona a una serie di server DNS uguali che lavorano in parallelo. Inoltre molto spesso si hanno numerosi server di backup.

Ciascun ISP ha un DNS locale, detto anche **default name server**. Questo server serve a tradurre localmente se si possiedono le informazioni, oppure a chiedere a dei server di più alto livello. Questa pratica è utilizzata ancora una volta per velocizzare l’accesso al servizio e per togliere carico ai server root. DNSQuesto server non compare completamente nella gerarchia dei server DNS.

Un altra usanza molto comune è quella di mantenere la cache in locale degli indirizzi dei server *top level domain*, in modo da non dover sempre chiedere la risoluzione al server radice.



### DNS query

Ci sono due metodi per fare una DNS query:

* **query iterativa**: il server risponde con le informazioni se le possiede, se non le ha risponde con le informazioni per dove recuperarle. Spesso queste tecniche permettono al server anche di imparare questo valore in una sorta di cache.

  Questa richiesta risulterà come una serie di richieste fatte dallo stesso server in direzioni diverse, finché non ottiene la risposta cercata.

  Le informazioni in cache vengono invalidate dopo un certo valore di tempo.

  Spesso la macchina che esegue questa query ha un indice aggiornato di tutti i TLD server DNS per eseguire la ricerca più velocemente.

  I meccanismi di notifica e aggiornamento sono regolati nel RFC `2136`.

  ![image-20211020142453442](image/image-20211020142453442.png)

  

* **query ricorsiva**: questa modalità fa in modo che ogni server a cui si richiede porti una risposta, ovvero se il server la possiede comunichi quel valore, se non possiede la risposta allora si incarica di fare un’altra richiesta a un server che potrebbe sapere la risposta. In questo modo si crea una catena di richieste che produce un risultato solo all’ultimo passaggio, proprio come la definizione di chiamata ricorsiva.

  Questa modalità permette di salvare questo record di dati su tutta la serie di server passati per ottenere la risposta, fornendo potenzialmente a più server la possibilità di una risposta in più.



### Database DNS

Siccome il DNS è un database, studiamo insieme le informazioni che contiene, chiamate **RR (Resources Record)**. Questo record di risorsa ha 4 parametri: `(NAME, VALUE, TYPE, TTL)`, dove `TTL` è il *time to live* parameters, che indica dopo quanto tempo la risorsa deve essere tolta dalla cache. Il valore di `NAME` e `VALUE` dipendono dal parametro `TYPE`:

* `TYPE=A`: `name` is the *hostname* e `value` è l’indirizzo IP dell’host.

  Questo tipo di record quindi fornisce il tipico link tra indirizzo e hostname. Esempio:

  ```http
  (relay1.bar.foo.com, 145.37.93.126, A) 
  ```

* `TYPE=NS`:  `name` è il dominio, mentre invece `value` è l’indirizzo di un authoritative server che può sapere dove recuperare la risposta.

  Questo tipo di record serve quindi a far continuare la catena di query per portare una risposta all’host richiedente. Esempio:

  ```http
  (foo.com,dns.foo.com, NS) 
  ```

* `TYPE=CNAME`: `value` è alias di qualche nome canonico (ovvero `name`, il vero nome), `CNAME` appunto.

  Questo tipo di record permette aliasing dell’hostname, ovvero il fatto di nascondere un nome dietro un altro di più immediata comprensione. Esempio:

  ```http
  (foo.com,relay1.bar.foo.com, CNAME)
  ```

* `TYPE=MX` : `value` è il nome canonico del server di posta associato a `name`.

  Questo tipo di record permette ai server di posta di avere dei semplici alias. Esempio:

  ```http
   (foo.com, mail.bar.foo.com, MX)
  ```

  

### Messaggi DNS:

I messaggi scambiati con il protocollo DNS sono impostati con lo stesso formato, sia che siano richieste (query), sia che siano risposte.

![image-20211020152314135](image/image-20211020152314135.png)

![image-20211020152401509](image/image-20211020152401509.png)



### Inserire record nel database DNS

Si presuma che tu abbia un nome con cui vorresti fare un sito per la tua nuova startup, chiamato `new-soc.it`. Per inserire questo record ci si appoggia a un’entità chiamata **registrar**, che si occupa di controllare l’unicità del nome del dominio e in caso aggiungerlo ai database. Per questo servizio spesso si chiede di pagare una piccola parcella.

I registrar sono accreditati dall’Internet Corporation for Assigned Names and Numbers (ICANN), che dopo gli anni 90 ha permesso a più privati la partecipazione a queste azioni.

Quando si inserisce un valore nel DNS, si deve anche inserire il TLD della compagnia, sia il valore predefinito, sia il backup. Di questi compiti di solito se ne occupa il registrar.

Di fatto quindi, il registrar creerà 3 tipi di RR:

```http
(dns1.networkutopia.com, 212.212.212.212, A)
(networkutopia.com, dns1.networkutopia.com, NS)
(networkutopia.com, dns2.networkutopia.com, NS)
```

Chiaramente se si vogliono anche gli alias per mail e hostnames, allora si devono aggiungere altri dati (azione sempre effettuabile attraverso registrar) e inserire i valori `CNAME` e `MX`.

 

## 2.6 Applicazioni P2P

Le applicazioni P2P (peer-to-peer) sono state viste brevemente precedentemente [qui](#accesso-alla-rete), ma si possono riassumere con:

* nessun server è sempre attivo
* coppie arbitrarie di host e peer comunicano direttamente tra loro a livello applicazione, ovvero i router che permettono di effettuare gli “hop” fisici non si “notano”, ma si ottiene soltanto una scatola nera che entra da una parte e che esce dall’altra
* i peer non sono sempre online, non sono sempre nella stessa posizione in rete e non hanno sempre lo stesso indirizzo IP. La rete funziona anche se non tutti i peer sono online (entro certi limiti dovute al livello di richieste in entrata e potenza dei nodi attivi logicamente).
* La rete P2P funziona attraverso la creazione di una rete overlay, costruita da coppie di host che comunicano direttamente tra loro.

La connessione P2P è diventata molto utilizzata negli anni a cavallo del 2000 e per una serie di condizioni che la rendevano migliore per le caratteristiche della rete di allora. Dopo negli anni le caratteristiche di gestione hanno fatto in modo che il sistema preferito diventasse via via quello del client server, fino ad arrivare al cloud elastico.

Per capire i lati positivi di questa rete, analizziamo dei casi diversi.

Mettiamo che un server abbia da consegnare un file molto grosso, mettiamo il kernel di Linux. Se si applica la tecnica client-server allora la copia di questo file deve essere scaricata singolarmente da ogni client. Questo vuol dire che il server ha la necessità di avere una banda condivisa e di conseguenza per svolgere queste operazioni anche delle risorse condivise tra i client che fanno richiesta. 

In un classico caso di connessione P2P il server fornisce il file completamente solo a pochi peer (scelti in base a una serie di regole predefinite). Una volta che il file è stato distribuito a più host, allora anche questi possono partecipare alla condivisione del file aiutando il server fornendo delle parti di file. Questo processo è vantaggioso siccome un peer può essere più vicino dal punto di vista geografico e quindi avere la necessità di meno *hop* per concludere la trasmissione. Un altro punto di vantaggio è che si toglie la possibilità che un collo di bottiglia limiti tutta la condivisione: l’algoritmo di gestione dei peer in questo caso semplicemente redirezionerà la connessione attraverso un peer che fornisce una banda di accesso maggiore.

Queste immagini riassumono queste due condizioni e aggiungono i calcoli matematici che provano questa teoria:

![image-20211018154717895](image/image-20211018154717895.png)

In questo modo possiamo prendere un tempo per distribuzione di un file tra **client e server** di dimensioni $F$ a $N$ client come  $d_{cs} = max(NF/u_s, F/min(d_i))$. Questa relazione indica che il tempo massimo impiegato per questo trasferimento è il massimo tra il tempo del server per inserire i dati in rete e il tempo massimo che il client impiega a scaricare il file. Chiaramente in un caso ideale in cui tutti i valori si riducono a valori infinitesimi si otterrà che la crescita di questo valore dipende solamente da $N$. Per questo motivo il modello di fornitura di un file con l’approccio client-server è lineare.

Con il modello **peer to peer** la discussione è diversa: il server non è mai responsabile di tutta l’operazione di scaricamento (a meno che non sia la prima copia) oppure fornisce solo una frazione del file (che quindi vuol dire che il blocco dati mandato è più piccolo del file totale). Il server deve inviare una copia nel tempo $F/u_s$, mentre il client impiega un tempo $F/d_i$ per il download. Il tasso di upload quindi diventa distribuito su tutti i peer, avendo valore: $u_s + \sum u_i$. Questo sta indicare che il server condivide lo scaricamento del file con altri utilizzando connessioni multiple e fatte con hosts diversi, riuscendo a scaricare il file teoricamente più velocemente.

Il calcolo temporale in questo caso diventa: $d_{P2P} = max(F/U_s, F/min(d_i), NF/(u_s + \sum u_i))$. Questo calcolo significa che il termine massimo di aumento è la sommatoria (somma di termini dipendenti da $n$). Questo valore aumenta meno linearmente, al limite diventa asintotica alla funzione logaritmica. 

Confrontando le funzioni di incremento del tempo necessario per lo scaricamento di un file, avendo definito le costanti della relazione, si ottengono questi due modelli:

![image-20211018155714722](image/image-20211018155714722.png)

Di fatto quindi il P2P si ottiene un tempo di distribuzione asintotico in base all’aumentare di $n$. Questa analisi server a far vedere che in teoria il P2P è molto più adeguato ad un interner scalare: è capace di fornire un servizio con una velocità minima costante a tutti gli host, dopo un certo valore limite di peer connessi.

Ma il P2P ha anche dei lati negativi:

* il numero di peer necessario a ottenere dei tempi confrontabili con l’approccio client server nella vita reale è piuttosto importante e non sempre è raggiunto da queste reti (rendendo l’accesso ai file molto più lento alla fine, diventando molto simile al modello lineare)
* si presume che i peer siano sempre connessi e pronti, che forniscano un aiuto alla rete dopo aver terminato il collegamento. Queste pratiche di solito non vengono rispettate, spesso infatti ci si ritira dalla rete prima di ricondividere il file a sua volta, annullando quindi il concetto principale dell’esistenza di questa rete. Questa pratica di solito viene indicata con la rottura della catena di connessione.
* la manutenzione di questo modello è molto complicata, perché un aggiornamento del server implicherebbe il contattare tutti i peer.



### BitTorrent: un esempio di rete P2P

BitTorrent è un tipo di connessione P2P molto vantaggioso, siccome ha applicato una serie di modifiche utili per semplificare l’accesso e la gestione della rete. In primo luogo utilizza un server, chiamato **tracker** che fornisce una lista di peer che partecipano alla connessione. Gli altri peer che partecipano alla connessione si chiamano torrent.

Il funzionamento ad alto livello è questo:

*  il file viene diviso in più pezzi (chiamati **chunk** e di massimo 256kb), disposti in nodi della rete e duplicati in altri.

* si scarica un file chiamato manifesto: `manifest.torrent` che contiene l’indice dei peer su cui sono presenti le parti di cui è composto il file richiesto.

* attraverso una serie di controlli e parametri, il client torrent capisce a quali parti del file dare la precedenza per il download e quali nodi preferire per lo stesso chunk (anche in questo caso fa delle scelte in base alla velocità di accesso al file e l’eventuale presenza di un collo di bottiglia su uno dei percorsi). Questo algoritmo deve tenere conto di molti fattori:

  * Il numero di connessioni attive tra host e vari peer deve essere bilanciato: avere più connessioni attive nello stesso istante permette di continuare a scaricare il file e di essere veloce anche se mentre si sta scaricando da un nodo questo va offline,  ma allo stesso modo di mantenere abbastanza banda ancora utilizzabile. Un ultimo punto di cui tiene conto è la potenza del computer, infatti non tutte le macchine hanno uguale potenza e di conseguenza non possono servire tutte lo stesso numero di hosts.

    Nel caso di questo algoritmo di solito si mantengono sempre 4 connessioni, chiamate *favorite* decise in base a questi parametri e molti altri. A lato di queste si mantiene una sottolista di possibili nodi di backup nel caso uno dei 4 favoriti si facesse indietro.

  * i nuovi algoritmi di solito preferiscono chi offre maggiore capacità, rispetto a chi mette a disposizione poco. Questo è fatto in modo che le informazioni siano sempre più disperse per la rete e sempre più comuni a più peer.

  * l’algoritmo di solito preferisce i pacchetti disponibili su meno peer possibili: da una parte bisogna scaricarli subito in modo che se questi nodi in un momento siano offline io non mi trovi senza nodi da cui scaricalo. Dall’altra parte in questo modo, l’host su cui sto scaricando questo *chunk* *raro* può a sua volta iniziare a distribuirlo, rendendo più grande la rete di nodi disponibili a fornire questo pacchetto. Questo approccio è chiamato **rarest-first**.

* quando un peer ottiene un file (se decide continuare a partecipare alla rete), allora gli viene assegnato il compito di fornire un determinato chunk a chi si dovesse collegare richiedendolo.

Questo modello di servizio P2P punta a risolvere il problema di come trovare le informazioni sulla rete. Questo problema è stato particolarmente studiato e ancora ora viene studiato per fornire un’implementazione ottimale (che comunque è difficile). Nessun metodo di ricerca riesce a scalare bene in qualsiasi caso comunque. Per questo motivo avere un server che mantiene tutte le informazioni sul dove trovare i chunk necessari per scaricare il file è una possibile soluzione.

Dopo questa applicazione moltissime altre hanno seguito questo modello: centralizzato per la parte generale e di gestione della rete, distribuito per fornire i file richiesti.

Sempre più spesso la parte di login alla rete viene contralizzata, mentre la fornitura di elementi multimediali viene distribuita attraverso peer.



### Napster

Questa applicazione molto diffusa tra gli anni 2000 è stato forse una delle prime applicazioni della connettività P2P con ottimi risultati. Di più [qui](https://en.wikipedia.org/wiki/Napster).

Il funzionamento generale si basava su un server principale che manteneva le informazioni sulle informazioni memorizzate sui peer su un *server centrale*. Ogni volta che un peer si collegava alla rete, informava il server centrale e riceveva informazioni sui i file che stava cercando.

Questo servizio quindi utilizzava il P2P solo per la distribuzione dei file musicali che offriva, ma sfruttava l’approccio client-server per tutti i servizi necessari, come il servizio di localizzazione delle risorse. 

Questo servizio però poi ha avuto problemi perchè:

* si formava un collo di bottiglia sul server centrale (che aveva difficoltà a scalare, rispetto al resto della rete)

* il server centrale aveva le identità di tutti i peer partecipanti alla rete (questo elemento è anche quello che dopo ha permesso la chiusura di molti dei sui servizi in modo rapido, siccome erano stati accusati di promuovere il pirataggio della musica).

  

### Query Flooding

Questo nuovo protocollo si basa su un sistema completamente distribuito senza la necessità di nessun server centrale. E’ un protocollo pubblico utilizzato anche da Gnutella. Ciascun peer *da solo* indicizza i file che rende disponibili per la condivisione. Questo file viene mantenuto solamente in locale.

Ogni peer si collega a circa una decina di peer vicini per mantenere attiva la connessione e peter ricevere richieste dalla rete.

Ogni volta che si cerca un file viene eseguita una query (molto simile a quella del DNS idealmente): ogni peer se può rispondere con delle informazioni sul file cercato allora risponde, se non può allora rimanda la richiesta ai suoi nodi con la speranza che uno contenga questo file.

Di solito c’è il cosidedetto *raggio limitato* ovvero il segnale si può trasmettere solo ad un  numero di salti (ovvero la query non può circolare continuamente nella rete, ma una volta raggiunto un certo numero di hosts si fermerà). Questa protezione serve a evitare che la rete sia ovunque piena di query, oppure che una richiesta di un file non esistente continui a propagarsi per la rete. Per questa struttura di richiesta la rete viene appunto chiamata **query floating** a raggio limitato(ovvero si “inonda” la rete di richieste per trovare il file).

Una volta che la query ottiene un risultato, allora si crea una via unica scelta in modo che sia più veloce possibile. Questa via diventerà il tunnel che permetterà il completo scaricamento dei file (logicamente anche in questo caso questi valori sono divisi in chunk).

Per mantenere la copertura al meglio, allora ogni nodo è:

* leader
* assegnato a un leader

In questo modo, il nodo leader riesce a mantenere le informazioni che contengono i suoi nodi “figli” in modo opaco. Facendo così le informazioni vengono avvicinate sempre più e quindi si permette di ridurre l’overhead della richiesta sulla rete e soprattutto di avere più possibilità di ottenere una risposta.

Questo modello permette di ridurre le persone che conoscono tutti i dati, che è l’informazione più sensibile, ma allo stesso tempo fornire un servizio completamente distribuito e puntuale nel servire i file.



### Skype

Questo protocollo non è di pubblico dominio, ma è stato dedotto attraverso l’analisi del traffico che questa applicazione crea. 

Come gli esempi precedenti il modello di base su cui si basa il suo funzionamento è quello di fornire un modello P2P per la comunicazione fra utenti e un server centrale per indicare (come un indice) gli indirizzi dei singoli host con cui ci si vuole collegare. La copertura di questo indice viene mantenuta avendo un rete P2P non piatta, ma con alcuni nodi leader che si chiamano **super nodi**. Questi permettono ai vari nodi dispersi sulla rete di essere il più reattivi possibili nelle risposte e (ancora una volta) di poter evitare eventuali colli di bottiglia.

Il server centralizzato che gestisce l’indice è anche quello responsabile delle azioni di login degli utenti.

Un problema però che ha avuto questa applicazione è la sempre più frequente presenza di un NAT sulle reti domestiche. Il **NAT (Network address translation)** è un sistema utilizzato per limitare che host esterni alla rete domestica si colleghino direttamente a un host interno (sfruttando il concetto di IP pubblico e IP privato/interno). In questo modo gli host presenti all’interno della rete sono “mascherati” dall’unico host di uscita che li raggruppa tutti (metodo efficiente per la sicurezza di una rete e per altri motivi che si vedranno più avanti). 

Questo sistema, nonostante i molti lati positivi, impedisce di fatto la creazione di questo sistema, perché gli hosts privati non riescono a esporre al pubblico il proprio indirizzo IP. Questo problema Skype lo ha risolto con la creazione di *relay*. I relay (spesso situati anche in prossimità dei supernodi) permette a due hosts privati di connettersi tra loro: il primo host si connette al relay, il secondo host si connette al relay e il relay si occupa di connettere questi due utenti. Di fatto il relay agisce da *bridge* e sono situate in posizioni importanti della rete per permettere di evadere il NAT. 

Per i blocchi imposti dal NAT e la necessità di utilizzare il ralay in corrispondenza del supernodo, se il nostro supernodo di riferimento cade, allora cade tutta la connesisone e non si riesce a ricreare se non utilizzando un altro route.

[^Gabri dice di inserire queste parole.]: La lavatrice parla con internet io mi fumo l’alloro.



## 2.7 Cloud Computing

Il cloud computing è un estremizzazione del modello client-server, con una rivoluzione della gestione del server. 

Per questo discorso si utilizzano dei termini specifici: 

* gestore dei server o amministratore: è la persona o (più spesso) l’azienda che ha l’accesso ai servizi fisici e fornisce la manutenzione fisica del datacenter, oltre che supporto in caso di problemi tecnici.
* cliente amministratore: persona o azienda che affitta i server di un datacenter per fornire la propria applicazione.
* utente finale: si interfaccia con l’applicazione senza notare la differenza di macchina fisica sul quale sta lavorando oppure notando la differenza di carico che un determinato servizio sta subendo.

Le idee che stanno alla base di questo modello sono:

* le aziende che possiedono il datacenter dispongono di un grandissimo numero di server reali, di solito con un architettura a alta affidabilità e posizionati nei punti più favorevoli della rete in modo da avere la rete di accesso migliore possibile.
* il gestore del server espone i servizi attraverso un layer di applicazione che permette il funzionamento dell’elastic cloud, ovvero:
  * la possibilità di aggregare in maniera virtuale una serie di servizi in base alla capacità che si vuole fornire ai propri clienti, oppure (caso molto più probabile) in base alla necessità e al numero degli users richiedenti il servizio.
  * questo sistema permette di disattivare questi spazi virtuali se in numero troppo elevato rispetto agli utenti, ma permettono di crearne in  in caso di un picco sostenuto di traffico 
  * l’applicazione di questo servizio è fatta in modo che il cliente amministratore non si accorga della differenza dal lavorare in un servizio bare-metal a un servizio in cloud.
  * questi elementi permettono di avere dei servizi che sono estremamente scalabili e riescono a distribuire le risorse e il carico in modo estremamente vantaggioso.
* il risultato finale di tutti questi elementi è che l’utente amministratore può selezionare un servizio completamente componibile a cui i clienti possono interfacciarsi con estrema facilità e stabilità.
* Tutte le operazioni che includono il backup periodico, la salvaguardia dei dati, il controllo del benestare dei server, il controllo del corretto funzionamento e la manutenzione in generale è completamente lasciata in mano agli amministratori, togliendo questi problemi dai pensieri del cliente amministratore, che deve soltanto pensare all’effettivo servizio che vuole offrire.



Nonostante i lati positivi indubitabili del sistema in-cloud, ci sono alcune criticità non sottovalutabili che porteranno a un successivo miglioramento di questo servizio: 

* problemi di **sicurezza e privacy**: i dati sono dispersi all’interno del datacenter, non localizzati in modo preciso.

  Nuove tecniche permettono di distribuire i dati solo su una serie di macchine predefinite in partenza oppure scegliere un luogo geografico dal quale non si deve uscire.

* problemi **internazionali di natura economica e politica**: siccome i datacenter diventano dei grandissimi agglomerati di dati, ogni servizio pubblico che si sposta in cloud oppure i singoli cittadini vogliono sapere che i dati siano mantenuti in un luogo che ha delle regole per il mantenimento dei server simili al paese di provenienza. Di fatto l’Europa sta cercando di dotarsi di datacenter completamente europei e quindi sotto le regolazioni del GDPR.

  Allo stesso modo, siccome anche i servizi pubblici è sempre più facile trovarli come servizi in-cloud, i singoli governi (che vogliono mantenere il controllo su queste informazioni sensibili) hanno la necessità di creare datacenter geo-politicamente localizzati.

* continuità del servizio offerto: gli amministratori devono gestire tutte quelle operazioni di manutenzione e backup (come detto appena sopra) per mantenere il servizio online e ben funzionante. 

  Questo tipo di sicurezze dipendono del tutto in base alla compagnia alla quale ci si sta affidando.

* migrazione dai servizi: è sempre più comune la pratica del *vendor-lockin*, ovvero un cliente che ha tutti i sistemi impostati per lavorare con una determinata azienda fornitrice ha difficoltà a cambiare fornitore di servizi perché avrebbe la necessità di rivedere molti aspetti pratici del funzionamento del sistema di elastic-cloud, siccome sono tutte layer applicativi diversi e non compatibili.

  Negli ultimi anni si sta osservando una rivoluzione da questo punto di vista, siccome molti software e tecniche utilizzate stanno venendo generalizzate e standardizzate, sfruttando del codice in comune ai produttori.



Chiaramente il processo di migrazione dal modello client-server al modello in-cloud non è stato immediato, ma ha passato una serie di fasi intermedie che stanno ancora oggi modificandosi e aprono nuove possibilità a questo modello in futuro.

![image-20211018165716454](image/image-20211018165716454.png)

![image-20211018165842169](image/image-20211018165842169.png)

1. Chiaramente all’inizio si utilizzavano una serie di macchine fisiche, dove si facevano girare dei servizi direttamente sul server, come fosse un normale terminale.

2. I servizi forniti attraverso macchina virtuale diventano sempre più utilizzabili perché iniziano a fornire un modello di scalabilità accettabile per il tempo

3. I servizi vengono forniti attraverso i container, ovvero delle macchine virtuali completamente minimizzate e altamente ottimizzate, studiate per contenere una singola istanza dell’applicazione. La creazione di questi container è pressoché immediata e permette la completa duplicazione del servizio.

   Attraverso un load-balancer questi sistemi possono gestire una grandissima mole di traffico sfruttando qualche instanza dell’applicazione lanciata in container.

4. In futuro vedremo delle applicazioni create in tempo reale al momento della richiesta per svolgere una singola funzione in modo completamente ottimizzato (Amazon nel suo cloud AWS permette questo servizio attraverso le *lambda functions*). Questo modello si chiama **serverless** e occupa sul server la potenza necessaria solo per svolgere una singola azione su richiesta.

Chiaramente questo andamento (come si nota anche in figura) ha permesso una scalabilità sempre più granulare e ottimizzata, ma allo stesso tempo ha portato a una grandissima dispersione delle risorse dato dall’overhead di questi programmi per la gestione di container o applicazioni immediate.

La granularità della scalabilità del servizio si nota sopratutto nell’ultimo servizio, il serverless. Questa applicazione permette di avere delle funzioni create ad hoc invocate e create singolarmente ogni volta che sono necessarie, per poi essere distrutte.

Un immagine che sintetizza questo discorso è la seguente:

![image-20211020200020907](image/image-20211020200020907.png)



### La rete del Datacenter

Di un datacenter la parte più complicata è la rete. Questa parte è logicamente la più importante e quella che necessita di più attenzione, anche perché è pensata per avere degli obiettivi diversi da tutte le altre reti esistenti.

I servizi all’interno del datacenter devono avere più banda di accesso possibile e essere sempre online, di fatto quindi questa rete deve fornire numerosi collegamenti di accesso per fare in modo che la linea di collegamento non sia unica. In questo modo le connessioni hanno un backup, riescono a smaltire il traffico attraverso altre vie se si trova un router congestionato nel percorso, inoltre si cerca di avere la banda di accesso più grande possibile per ogni server esposto sul web.

Dall’altra parte il datacenter si basa anche sulla gestione delle tecnologie di virtualizzazione. Queste tecnologie, prima relegate solo agli spazi fisici dei server ad alte prestazioni, stanno arrivando sulla rete attraverso la virtualizzazione delle reti. Queste tecniche permettono di creare, di fatto, una rete virtuale di overlay basata su delle effettive tratte di rete.

Essendo il networking così importante per il datacenter, si ha un organizzazione granulare delle connessioni e dei router principali:

![image-20211018170724860](image/image-20211018170724860.png)

Gestire una rete così grande, evitando blocchi, sovraccarichi o congestioni è veramente complicato. Bisogna aver fatto uno attento studio sull’instradamento per ridurre la possibilità di congestione. Per garantire la continuità del servizio si cerca la ridondanza tra tutti gli apparecchi (motivo per cui i router principali sono tutti collegati tra loro a stella, a differenza delle reti esterne nel quale si cerca di minimizzare le tratte fisiche). L’unico particolare che facilita il lavoro di gestione di una rete così impegnativa è che tutti questi apparecchi sono tutti dello stesso proprietario e useranno per lo più la stessa tecnologia. In questo modo interfacciarsi e intervenire è molto più facile e generalizzabile per i manutentori.

Il datacenter ha un accesso alla rete a alta capacità e banda larga. Questa connessione viene poi distribuita nel datacenter, spesso con dei collegamenti anche più larghi dell’effettiva banda di accesso passando prima per i **core-network** router, che permettono i collegamenti principali con tutti gli altri router. Questi core-networks router si occupano di collegare aggregation-network. Infine di solito ci sono gli access-network router, che spesso si trovano nei rack (gli “armadi”) per i server.



### Video Streaming e CDN

Un esempio di servizi spesso forniti da un datacenter che ha un ingombro di banda il più grande possibile è lo streaming video. Questi servizi hanno delle necessità di traffico e di banda in uscita dal datacenter molto alto, tanto che si stima che l’80% del traffico di rete mondiale ormai sia prodotto da servizi di streaming come YouTube, Netflix o AmazonPrime.

Per gestire un servizio del genere una sfida è essere capaci di scalare le risorse in modo automatico in base al numero di utenti, in modo da fornire un servizio consistente a tutti i clienti.

L’altra sfida è l’eterogeneità delle reti di accesso degli utenti che accedono al servizio. Le bande di accesso sono molto fluttuanti e variabili, possono avere dei rallentamenti e dei colli di bottiglia e soprattutto possono avere caratteristiche diverse. Ad esempio la rete cellulare ha una banda piuttosto larga se non è congestionata, ma è molto instabile e quindi fornisce prestazioni molto variabili. La rete in fibra dall’altra parte fornisce un servizio a banda larga con un estremo grado di affidabilità. La sfida quindi è garantire un servizio molto simile a questi due utenti nonostante le differenze tecniche sostanziali.

Un modo per risolvere questo problema è attraverso l’utilizzo delle **CDN (Content Delivery Network)**. Questa infrastruttura è una rete overlay dedicata di un azienda, oppure fornita a un azienda che ne ha necessità da un terzo e si basa su una rete dedicata di datacenter di dimensioni più ridotte. Questi piccoli datacenter vengono chiamati **content delivery node** e seguono le stesse regole per il posizionamento dei normali datacenter: vicino ai grandi nodi con banda di accesso molto larga. La differenza che contraddistingue i CDN dai normali datacenter è che, essendo più piccoli e più dispersi per il territorio, vengono posizionati in modo da essere il più vicino agli utenti finali possibile. In questo modo si possono bypassare eventuali colli di bottiglia nel percorso dal datacenter all’user e permettere quindi una maggiore velocità di accesso a questi servizi.

Il funzionamento dei CDN è strettamente collegato a quello del datacenter centrale: sui nodi si caricano una serie di file che vanno condivisi. Il client verrà redirezionato verso questi CDN rispetto che al server centrale al momento di necessità di accesso al servizio, facendo in modo che il cliente scarichi direttamente da questi server più vicini.



#### CDN: applicazione

Per fornire una rete di CDN si possono utilizzare più strategie per disporre i server nelle nazioni necessarie. Questo servizio può essere “affittato” da aziende che fanno questo di mestiere, come Akamai, oppure si può decidere di creare un sistema “fatto in casa” come la rete di CDN di Google (che serve anche YouTube).

Le reti di CDN possono essere organizzate in due modi diversi, principalmente:

1. *vai in profondità* (**enter deep**): questa tecnica di fornitura dei server si appoggia direttamente agli ISP di accesso alla rete. Con questi provider ci si accorda per avere dei server cluster disposti nella loro rete di accesso. Si ottiene quindi una rete di server disposti tutti sulle reti di accesso, ma estremamente frazionati (si parla, nel caso di Akamai, pioniere di questa tecnica, di circa 240000 server in più di 120 nazioni). Questi nodi hanno la caratteristica di fornire una banda di accesso e un throughput ai clienti molto basso, ma il fatto che forniscono un servizio così distribuito rende la manutenzione e la gestione un grandissimo problema. Per questo motivo queste aziende hanno servizi di monitoraggio della rete e dei server molto avanzati, con la possibilità di avere dei dati aggiornati in tempo reale e statistiche sull’andamento. Inoltre, siccome un servizio di un azienda è così disperso per il mondo, spesso vengono usati questi server come indicazioni dell’andamento mondiale della connettività, in modo da capire in tempo se una zona geografica è effettivamente disconnessa dalla rete.

2. *porta a casa* (**bring home**) questa seconda filosofia, applicata da altri esperti del settore e fornitori di CDN, si basa sulla creazione di datacenter più piccoli, che di solito vengono connessi dall’ISP o sono posizionati presso grandi POP, ma non lo sfruttano direttamente, posizionandosi completamente all’interno della rete. Questa tecnica si può anche pensare come si avessero dei grandi Exchange Points (IXPs). 

   Questa filosofia, rispetto a quella *enter-deep* permette di avere una necessità di mantenimento e gestione minore, ma allo stesso tempo fornisce agli users un throughput e una banda ridotta rispetto all’altra tecnica.

Il CDN di grandi servizi, che non possono permettersi di avere l’intero database su tutti i nodi funziona molto più come un server di cache. In questo modo, al momento della richiesta da parte del client, si ottiene un *manifest* che indica i server CDN disponibili per lo scambio. Il client identifica il server migliore con una serie di test automatici, e avvia lo streaming video. Se questa risorsa richiesta si trova già sul nodo, allora viene recuperata e manda all’utente. In caso contrario nello stesso momento della fornitura del video ai clienti, una copia viene salvata sul server per successive richieste.

Il funzionamento di queste reti spesso è permesso da una serie di DNS server studiati per direzionare il traffico in base alla localizzazione del pacchetto che richiede. In questo modo, il DNS penserà in automatico a dividere le richieste tra i server CDN più vicini, permettendo una maggiore divisione delle risorse.

##### Study Case

Servizi come **YouTube** che non possono permettersi di avere l’intero database di risorse video che possiedono sulla piattaforma copiata su ogni CDN. Per questo utilizza una serie di tecniche statistiche e di (possiamo dire) azioni simili al funzionamento della cache che permettono di avere in questi nodi solo i video più visti e più cliccati nella regione che questo nodo serve (i video più popolari). In questo modo, i video più popolari al momento nella zona sono anche quelli con velocità di accesso maggiore, mentre invece quelli meno popolari hanno una maggiore latenza.

Altri servizi, non propriamente di streaming, possono sfruttare questo servizio per migliorare la reattività delle loro applicazioni. Ad esempio **FaceBook** ha la sua CDN per fornire video e immagini da risorse più vicine, fornendo un servizio più stabile e reattivo ai suoi clienti. Intanto fornisce i restanti servizi dal server centrale, siccome necessitano di meno banda e sono più veloci sulla rete.



### Multimedia: codifica e flusso di dati del video

Come ogni tipo di file, anche tutta la multimedia viene codificata con determinate regole e modelli. Di fatto il video è una sequenza di immagini visualizzata a frequenza costante, al minimo 24 imm/sec. Le immagini digitali sono costituite da pixel, che hanno una quantità di bit variabile in base alla qualità e la quantità di colori rappresentabili. 

Per capire il reale problema di trasferimento di questi file prendiamo come esempio un’immagine di 2MB. Questa immagine è una matrice $2 \times 10^3 \times 10^3$ bit, che indicherebbe per immagine media una quantità di circa $125$ pixel. Se questa immagine avesse la necessità di essere in un video, allora per avere una fluidità dell’immagine e continuità di visualizzazione, si necessiterebbe di una banda di accesso di `400Mbit/sec`. 

Logicamente questi parametri non possono essere raggiunti da tutte le linee di accesso, per questo motivo si è cercato delle tecniche che diminuissero questo ingombro di banda. Si ottengono queste caratteristiche con degli algoritmi di compressione che possono lavorare in modi diversi:

* possono ridurre la quantità di informazioni racchiuse in un immagine togliendo tutte le informazioni in eccesso. Questa azione viene svolta molto bene dall’algoritmo `jpeg` che appunto permette di sapere quali sono le zone ridondanti delle immagini.

  Una volta ottenuti questi dati, le zone ripetute vengono mandate una sola volta, all’inizio della sequenza, e poi modificate con delle indicazioni vettoriali specifiche.

  Per questo motivo, se il bit-rate di scambio dati diminuisce, allora si ottiene un’immagine “quadrettata” oppure con delle “scie” di quadretti. Questo avviene perché l’algoritmo non riesce più a ricostruire l’immagine perché ha troppi pochi dati.

* possono (in questo caso solo per applicazioni video) partire da un immagine iniziale e mandare ogni volta solo la differenza che questa possedeva con la precedente. In questo caso si ottiene una larghezza di banda necessaria molto variabile: alta se si necessita di mandare scene in cui c’è molto movimento, bassa se si riproducono scene meno frenetiche e più statiche. 

  Di solito DVD (lettori e codificatori) funzionano in questo modo. Hanno una frequenza di collegamento medio di circa `5Mbit/sec`, che può però variare fino a `10Mbit/sec` circa sulle scene più intense o sui tratti meno compressi. Se questa banda non è disponibile allora si ottiene un’immagine distorta oppure non nella qualità iniziale.

* anche i codificatori più moderni eseguono una compressione delle immagini, ma fanno in modo di mantenere più dati in maniera più consistente, per avere quindi un immagine a qualità più alta.

  Queste tecniche sono capaci di sfruttare la codifica spaziale e temporale di tutte le immagini, immagazzinando più valori (e garantendo una qualità migliore) ma con un’occupazione di spazio accettabile.



Sulla rete nessuno manda dei video in maniera non codificata, perché non è accettabile fornire dei servizi con questa necessità di banda su internet. Inoltre se ci si scambiassero i file multimediali in questo stato non compresso, si otterrebbe una rete molto spesso congestionata.

L’audio di solito è per applicazioni umane, per cui si cerca sempre di sfruttare la sensibilità ai diversi stimoli del corpo umano e in particolare dell’orecchio. Di solito si usano queste variazioni per mandare il segnale nel migliore dei modi, ovvero cancellando tutte quelle informazioni registrate dall’apparecchio che il nostro orecchio non potrebbe comunque avvertire. Inoltre, il nostro cervello riesce a ricostruire alcuni pezzi mancanti di informazione, se ne ha il contorno. Per questo motivo spesso, negli algoritmi di compressione musicale più spinti, si tolgono alcune informazioni, sapendo che il nostro cervello ricompenserà a questa mancanza.

Ci sono due modi per fare codifica dei file video: 

* **CBR (Constant Bit Rate)**: ovvero mandare un video con una quantità fissa di dati. Non è una tecnica utilizzata spesso, però è quella di più facile implementazione.
* **VBR (Variable Bit Rate)**: ovvero il sistema ha capacità di adattarsi e modificare la quantità di bit che scambia tra gli host, in un meccanismo molto simile a quello del DVD. Poi questo concetto viene sviluppato meglio dal protocollo [DASH](###streaming-multimedia:-DASH). 



### Cosa comporta fare streaming

Il flusso di dati necessario a scambiare le informazioni tra client-server sono molti e può succedere che per vari motivi si ottenga una larghezza di banda ridotta (di solito avviene per multiplexing statistico oppure semplicemente per congestione del server). Questo effetto inoltre si amplifica allontanandosi dal server da cui si sta recuperando il file.

Altri problemi di ricezione e invio di questi pacchetti possono essere:

* coda dei pacchetti su un determinato nodo studiata per dare la precedenza a altri tipi di dati
* semplice coda first-in-first-out che impone a questi dati di non essere subito smistati
* altri problemi della rete, che aumentano se si incontrano più colli di bottiglia del normale

Per risolvere in parte questo problema si utilizzano tecniche di streaming di video memorizzato, ovvero si utilizza un buffer prescaricato (una sorta di cache) di pacchetti sul client, in modo da garantire che, anche se la ricezione dei dati non è puntuale (si parla in questo caso di *jitter*, ovvero ritardo della rete variabile), si riesce a ottenere sempre una visualizzazione continua.

Queste tecniche inoltre permettono di avere un fast-forward e un fast-backward sul video molto più reattivo, senza la necessità di richiedere a ogni azione questi dati. Dall’altro lato però queste tecniche vengono interrotte e rese inutili da continui interventi dall’utente. Infatti se si continua a modificare la sezione da riprodurre il video risulterà più lento siccome si dovrà recuperare una sezione diversa da quella già scaricata in cache.

![image-20211019142747519](image/image-20211019142747519.png)



### Streaming multimedia: DASH

Per migliorare lo streaming e la ricezione di file multimediali (i più comuni degli ultimi anni) si hanno inventato delle tecniche diverse e dei protocolli appositi per migliorare l’utilizzo di questo servizio.

Il protocollo **DASH (Dynamic Adaptive Streaming over HTTP)** è stato sviluppato apposta a questo scopo, e siccome ha una serie di necessità di servizio molto strette è stato sviluppato appositamente. Chiaramente è servito attraverso il protocollo TCP siccome fornisce di default il servizio di congestions-control e recupero pacchetti persi, e ha un’altra serie di caratteristiche particolari. Dal lato **server**

* si produce un video viene diviso in *chunk* e si copiano tutti questi oggetti in tutte le qualità (e quindi bit-rate di dati) possibili forniti dal servizio.
* tutti questi dati vengono replicati sulle CDN (sfruttando logicamente la loro logica geografica di raggruppamento dei dati) in modo da avere i dati dal mirror più vicino possibile.
* spesso il server stesso invia nella prima richiesta un file di *manifest* che contiene tutte le informazioni per recuperare i vari chunk e nelle varie qualità

Dal lato **client**:

* si ottiene una stima della banda disponibile per ogni richiesta fatta al server. Questo valore avviene in maniera indiretta sfruttando il tempo di arrivo dei pacchetti e altri fattori specifici.
* al momento di recuperare i dati, si una richiesta che include il valore di banda appena stimata, in modo da avere come risposta il chunk con il rapporto qualità/disponibiltà del servizio più alta possibile. (insiema a questi valori si inseriscono anche altri, ad esempio qualità massima del dispositivo. Questo per fare in modo di non fornire dei dati che non verrebbero utilizzati)
* finita questa richiesta si riinizia questa procedura, con la possibilità di ottenere una stima della banda diversa e quindi ricevere un chunk differente.
* con queste tecniche si cerca inoltre di sfruttare nel modo migliore la cache per ottenere un playout costante.
* chiaramente un chunk di dimensioni minori rispetto a quello di più alta qualità ottiene un immagine peggiore.

Un tipico esempio di applicazione di questo protocollo è YouTube, nel quale si può notare, sopratutto se si sta lavorando su una connessione instabile, come il chunk ritornato dal server possa avere qualità variabile.

Un altro particolare interessante da notare dell’applicazione di questo protocollo è che ancora una volta si posiziona l’intelligenza del servizio sugli *edge-point* della rete, in modo da avere un servizio scalabile e modificabile in base alla disponibilità di banda che la rete in un determinato momento mette a disposizione.



Per aiutare il funzionamento di questo protocollo si utilizzano alcune accortezze pratiche, ad esempio fornire i video più popolari su tutte le CDN, in modo che si bypassino i colli di bottiglia. Chiaramente quando si parla di soddisfare milioni di utenti entra in gioco in modo preponderante questa *delivery-network*. Avere un server centralizzato in questo caso non è la scelta migliore per due motivi principalmente: 

1. le persone vicine ricevono un servizio migliore, rispetto a quelle lontane 
2. si evita di avere un *one-point-of-failure* su tutto il servizio, avendo potenzialmente dei problemi con la totalità degli users in caso di malfunzionamento

## 2.8 Programmazione delle socket (cenni)

Le socket sono il modo principale per chi sviluppa l’applicazione per interfacciarsi con la pila TCP/IP. è stata introdotta con questa pila negli anni 80, funziona in modalità client-server e server a creare un interfaccia tra un host e un’altra.

Si può chiedere una socket, ma poi viene gestita dal sistema operativo. Si possono chiedere due tipi di socket: 

* datagramma inaffidabile (basato sull’UDP)
* affidabile, orientata ai byte (TCP)

si dichiara un buffer, la socket poi si interessa di via via liberarlo. Questo può essere utile per non doversi appoggiare a altri protocolli a livello applicazione, ovvero a lavorare a livello un po’ più basso. 

Quello che appartiene a una certa socket vengono distinti attraverso una doppia tupla di (indirizzo, porta) sia per il ricevente sia per il mittente.

Ogni processo (o ogni multithread del processo) ha associato delle socket. Si hanno circa 60000 connessioni in parallelo, siccome la porta è un valore di 16bit. `(IP, port, IPs, port)`.

Chiaramente il server risponde sempre su porta 80, ma di solito dopo divide il traffico tra varie socket aperte su altre porte. Dopo se ci si accoda su questa porta e il server piano piano risponde. Di solito ci sono dei limiti pratici: la quantità di memoria e la banda del server che possono far bloccare le informazioni di passaggio dal server. In altri casi si può avere un’applicazione non solida e quindi può andare in crash.



Aprire una socket: si parte dall’host (crea una socket, associa una porta e poi si mette in ascolto sulla porta selezionata) `socket(), bind(), listen()`, attende richierste di connessione ....

-- Ci sono i lucidi per la programmazione in Python per la socket





# Capitolo 3: Livello di Trasporto

Gli obiettivi di questa sezione servirà a capire i principi che sono alla base dei servizi di livello di trasporto, quindi ad esempio:

* multiplexing/demultiplexing
* trasferimento dati affidabile
* controllo di flusso
* controllo di congestione

Descrivere protocolli del livello di trasporto di internet: 

* UDP: trasporto senza connessione
* TCP: trasporto orientato alla connessione
* controllo di congestione TCP

## 3.1 Servizi ...

Fornisce la parte logica della comunicazione tra processi applicativi di host differenti. I protocolli di trasporto vengono eseguiti nei sistemi terminali:

* lato invio: scinde i messaggi in segmenti e li passa al livello di rete
* lato ricezione: riassembla i segmenti in messaggi e li passa a livello di applicazione

Questo livello si occupa della comunicazione logica tra i processi. Fa in modo si possa creare un livello di rete per mettere in contatto processi. 

Questo livello non ha bisogno di conoscere gli indirizzi IP, ma utilizza le porte per capire che processo deve parlare con quale. L’analogia che viene fatta nel libro bisogna immaginare come due edifici: in entrambi ci sono diverse persone, e quando uno deve mandare le lettere in un edificio allora ha bisogno dell’indirizzo dell’edificio, poi ha bisogno del piano. Lo smistamento sui singoli piani è fatto al livello di trasporto. (un protocollo lavoro all’interno dell’host, l’altro lavoro tra gli host).



Protocolli del livello di trasporto:

* TCP: affidabile, consegne nell’ordine originario
  * controllo di congestione
  * controllo di flusso
  * setup della connessione
  * affidabilità della connessione. si può pensare come ci sia un vincolo di consegna.
* UDP: inaffidabile, consegne senz’ordine
  * estensione senza fronzoli del servizio di consegna best effort
  * bisogno dei dati rapidamente
* Servizi non disponibili:
  * garanzia di ritardi
  * garanzia sull’ampiezza di banda
  * non si immagini mai che uno sviluppatore sia in grado di dire se la rete è in grado di avere queste garanzie, ma la rete non collabora.

Demultiplexing: dividere le richieste in arrivo sulle varie socket. Per fare questo ha bisogno di un informazione che è l’indirizzo di porta. La prima riga dell’header del TCP sono 32bit, divisi in due campi da 16bit ciascuno. Il multiplexing e il demultiplexing viene fatta su questa base:

* primi 16bit numero porta di origine 
* ultimi 16bit numero porta di destinazione

**indirizzo IP non è in questo header, siccome non sono nell’ambito di competenza del livello di trasporto!!**

Questo livello non sa neanche come sono fatti gli indirizzi di reti e non deve neanche saperlo.

Ogni livello ha un task molto preciso, quello di questo livello è focalizzato sulle porte.

La socket UDP invece è identificata da due parametri (IP destinazione e porta di destinazione). Il buffer UDP, non avendo connessione, “scarica” tutto sullo stesso buffer, come se consegnasse tutto alla stessa porta.



## UDP: User Datagram Protocol

Si basa sul FRC `768`. è un protocollo non affidabile e senza fronzoli (aggiunge ben poco rispetto al livello di rete). L’unica cosa si aggiunge l’informazione tra e porte. 

Non fa handshake (non sa se il destinatario è pronto quando si inizia a mandare), si possono mandare pacchetti fuori sincrono, mancanti, oppure persi proprio.

UDP esiste perché sembra una banalità: 

* trasmettere dati rapidamente e molto semplici (infatti non mantiene lo stato e ha altre informazioni)
* header molto minimale
* latenza ridotta al minimo
* ha una sua utilità

Specifica del protocollo:

* n°porta di origine
* n°porta di destinazione
* lunghezza del messaggio (in byte del segmento UDP, incluso header)
* checksum 
* dati dell’applicazione

Si può utilizzare l’UDP ache per il DNS e l’SNMP, oppure di controllo per 

ORM Object ... => massima grandezza del pacchetto che si può inserire nel singolo pacchetto UDP. Poi si vede che non è molto utile fare pacchetti così grandi, ma poi vedremo.

`checksum`: allineate il segmento in una struttura più stretta

| PORTA Origine |
| ------------- |
| porta dest.   |
| checksum      |
| payload       |

Poi si fa `XOR` bit per bit (calcolare ce i bit saranno incolonnati per 16bit per riga, siccome la porta di dest, e la porta di orginie sono così). 

è una forma di hashing, anche se in questo modo non ci sono formule matematiche dietro questa formula. Il checksum si fa in questo modo perché è possibile capire se la sequenza mandata è corretta. UDP può buttare via, quindi se c’è un errore allora viene cestinato di solito. Se viene cestinato automaticaticamente. 

Il recupero degli errori è una materia molto sofisticata su alcune branche, in modo che si possa recuperare. 

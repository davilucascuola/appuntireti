(lezione 6)

# Perdita di pacchetti

Difficile perdere a causa del traffico, oppure con problemi di consegna. Di solito si perdono nei caso di congestione di pacchetti, segnalata da ritardi sempre più elevati. Quando questa situazione peggiora i buffer dei router si riempie e poi inizia a droppare i pacchetti se non ci stanno più cose.

La rete è delicata, infatti c’è un bilanciamento tra affidabilità (ridurre la possibilità di dati errati e la capacità di recuperarli) e la sostenibilità della rete (mantenere il collegamento funzionale al meglio).



# Throughput

Questo parametro valuta gli effettivi bit/byte per unità di tempo che si possono ricevere/mandare da mittente o ricevente. 

Si possono fare delle assunzioni abbastanza significative attraverso il **collo di bottiglia**.

La prestazione che si ottiene al massimo si ottiene calcolando il minimo tra i vari dati che si ottengono. Questo significa che il rallentamento peggiore è quello che alla fine regola la velocità del trasporto.

Viene fatto un calcolo di questo tipo per capire che prestazione fornisce una rete, in base a quanto io abbia bisogno di rete in un determinato momento.

Per risolvere questo problema si possono pensare a vie diverse per raggiungere quella posizione, oppure portare un miglioramento alla linea di contatto peggiore.



Questo valore si può misurare il throughput *istantaneo* o *medio*. (di solito si considera il valore medio). 

La misura istantanea si intende il throughput in un solo istante oppure nel mandare un solo pacchetto.

Per trovare il massimo throughput in un determinato collegamento si deve trovare il minimo tra le connessioni.

Dipende anche dalla condivisione delle risorse di un certo collegamento.



	# Livelli di protocollo

le reti sono molto complesse. 

Dagli anni 70 c’è una tassonomia della rete, che si rifà in qualche modo alla similitudine dell’organizzazione di un viaggio. Di solito si fanno una serie di operazioni elementari e sequenziali.

## Perché la stratificazione?

Quando si ha a che fare con un sistema complesso, la struttura esplicita dei vari componenti della struttura e delle loro inter-relazioni.

Il motivo di questa decisione viene spiegato da un punto di vista logico molto facile: un sistema complesso sarebbe difficile da creare se non si divide in pezzi e se ne fa un pezzo per volta.

Inoltre questa modularizzazione permette di separare e migliorare le funzioni separatamente.



Questo modo di fare è super efficiente normalmente, ma se si vogliono performance super allora questa struttura a strati non funziona. Di fatto essere super espandibile e aperta a qualsiasi standard a struttura non funziona sempre, ad esempio ai sensori (si deve togliere il sistema a strati per preservare batteria e migliorare le performance). 



## Pila di protocolli internet: Modello TCP/IP

A ogni livello appartiene una funzionalità particolare specifica, in modo che ognuno abbia il suo compito principale. Di solito l’utente o il software ce utilizza la rete si appoggia a questa pila, non ci entra in contatto direttamente. Questa pila viene offerta dall’interno del server e offre servizi std e funzionalità di base. Questi particolari sono direttamente nel sistema operativo e sono presenti in ogni macchina. Non si possono cambiare i modi con cui si interagisce con i livelli successivi o precedenti, ma si può aggiungere o sostituire qualsiasi livello a patto di mantenere la pila.

Pila dei protocolli:

1. **fisico**: questo livello è responsabile di convertire la sequenza di bit creata nel link come una frequenza d’onda che si può propagare. Questo è molto importante perché è l’effettivo funzionamento della rete.

2. **link**: instradamento dei pacchetti attraverso la serie di commutatori di pacchetto. Questo permette di avere un percorso fatto anche con tecnologie diverse, e si può utilizzare attraverso l’instradamento di pacchetto. 

3. **rete **: instradamento dei datagrammi dall’origine al destinatario, ovvero crea il collegamento per il livello di trasporto, creando la strada attraverso la ricerca tramite grafo. 

4. **trasporto**: il livello di trasporto costruisce quella connessione virtuale che mi permette di vedere direttamente il server, in cui non si percepisce la rete, ma si ottiene semplicemente il contatto che viene tipo “oscurato” come fosse un unico tunnel. Costruisce la connessione logica grazie anche al livello di rete

5. **applicazione**: supporta l’applicazione di rete, ovvero applicazioni che si interfacciano con la rete. Questo mi fornisce un linguaggio che so già che riuscirò a capire. Quando si sviluppa un applicazione che sfrutta la rete il sistema operativo fornisce questa interfaccia per interagire con la rete. Di solito questi livelli sono a strati, ma si può arrivare al massimo al livello di trasporto.

   FTP, SMTP, HTTP, HTTPS, ... (numerosi protocolli in questa sezione)

Tutti questi livelli comunicano tra loro e sono necessari per il funzionamento di tutta la funzione. Questo avviene in modo top-down-botton-up, che permette di discendere la pila da una parte e risalire dall’altra fornendo un servizio che poi in realtà risulta oscuro a chi lo utilizza, ignorando le altri parti della pila.



Ogni livello non fa altro che prendere i dati ottenuto di sopra e imbustarli in maniera diversa, scrivendo qualcosa che serve all’entità di trasporto da una parte per comunicare all’altra box. Questa intestazione, chiamata *header* viene creato e “aggiunto” al pacchetto, incapsulando un pezzo dentro l’altro. 

All’interno del percorso di rete si incapsula via via, fino a diventare un **datagramma**, dopo il livello 3, che ha due indirizzi: il livello di arrivo e il livello di partenza. Quando un pacchetto passa qui in mezzo non si controlla cosa ci sia dentro, ma si inserisce semplicemente in una cassa con questa intestazione particolare.

Il protocollo di link incapsula il datagramma di rete con l’header di link H per creare un **frame/trama**, responsabile poi di viaggiare verso la destinazione.

![image-20210928144728616](image/image-20210928144728616.png)

In questo modo si riesce a parlare con il destinatario, attraverso la creazione di un pacchetto che può cooperare con livelli di pila uguali. Questo è necessario per garantire la comunicazione.

Se ci sono dei problemi si devono implementare delle funzionalità, in modo che chi riceve non sa se è in frequenza o altro. In questo modo invece si abilità la comunicazione orizzontale.

Questa stratificazione a 5 livelli sono implementati in ogni HOST e funzionano in serie, uno sopra condivide con il livello sotto o il livello sopra. Una volta che si incapsula, per recuperare il messaggio serve a de-incapsulare.



## Modelli di riferimento ISO/OSI

Questo è un modello di standard ufficialmente costruito e pubblicato, nella realtà però poi la gente ha implementato l’altro.



## Come funziona se i due Host non sono direttamente collegati

![image-20210928145303344](image/image-20210928145303344.png)

Gli host sono gli unici che hanno tutti e 5 i livelli. Gli switch e i router di rete non hanno tutta la pila, perché non hanno la necessità di “spacchettare” tutti i livelli del messaggio, ma solo fino a quando trovano le informazioni che gli competono.

Lo **switch** può ricevere fino a livello 2, e poi ritrasmette fino a livello 5, poi manda il pacchetto re-incapsulando. Chi riceve questo pacchetto poi ottiene un pacchetto nuovamente ricomposto.

Il router per natura non girano applicazioni, non è un server e non è una componente attiva nei servizi, ma è solo chi smista il traffico, lavorando a livello 3.



## Sicurezza di Rete

Quando sono strati creati i primi protocolli le persone collegate non erano in pensiero per la sicurezza, perché gli unici che serviva erano quelli che utilizzavano e era impensabile che la attaccassero. 

Questo ha creato un problema dopo con la confidenziabilità delle informazioni oppure la privacy delle comunicazioni, e queste operazioni sono state inserite nel tempo dopo come “patch”, ovvero come cerottoni per risolvere dei problemi. 

I problemi più comuni sono:

* malware che può raggiungere host in modo nascosto e aprono la comunicazione per controllare il computer obiettivo (possono essere virus, worm o cavalli di troia)

* Malware per spionaggio che memorizzano informazioni del computer

* gli host possono essere raggruppati per creare un DDoS (Distruibuited Denial of Service) che sovraccarica un server attraverso un continuo mandare di segnali e pacchetti da una serie di host. ù

  un attacco distribuito è molto difficile da prevenire e comprendere, e di solito l’unico modo è blacklistare l’host che sta attaccando.

* malware è spesso autoreplicante, ovvero un host infettato può passare a altri host sulla stessa rete.

* packet sniffing: si tratta di intercettare i pacchetti. Di solito il traffico è in broadcast, ovvero ogni apparato riceve tutti i pacchetti ma accetta solo quelli destinati a noi. Di solito questi virus entrano in rete locale per copiare il traffico sfruttando il fatto che alcune sequenze di pacchetti conosciute sono utilizzabili (tipo l’attacco di playback: ripeto il pacchetto per avere di nuovo l’accesso). Questi software sono spesso utilizzati per preparare un attacco, non effettivamente come via di attacco.

  Questo attacco permette il *packet spoofing* ovvero l’imitazione di una serie di pacchetti con il fine di garantire l’accesso.





